tinymce.init({
    selector: 'textarea',
    height: 400,
    theme: 'modern',
    convert_urls: false,
    plugins: [
        'advlist autolink lists link image charmap preview hr anchor pagebreak',
        'searchreplace wordcount visualblocks visualchars code fullscreen',
        'insertdatetime media nonbreaking save table contextmenu directionality',
        'paste textcolor colorpicker textpattern imagetools codesample toc help'
    ],
    toolbar1: 'preview |insert | styleselect | bold italic | forecolor backcolor | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image | code',
    content_css: [
        '//fonts.googleapis.com/css?family=Lato:300,300i,400,400i',
        '//www.tinymce.com/css/codepen.min.css'
    ],
    /*file_browser_callback: function (field_name, url, type, win) {
        var myWindow = PopupCenter(APP_URL + '/admin/post/upload_file', 'Upload File Content', 1055, 688, field_name);
        win.document.getElementById(field_name).value = '';
    }*/
});