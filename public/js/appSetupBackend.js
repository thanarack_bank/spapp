$('.close').on('click', function () {
    $(this).closest('.message').fadeOut();
});
$('.ui.dropdown').dropdown();
$('select.dropdown').dropdown();
$('.ui.checkbox').checkbox({
    onChange: function () {
        console.log();
        var val = $(this).parent().find('input[type=hidden]');
        if (val.val() === 'on') {
            val.val('off');
        } else {
            val.val('on')
        }
    }
});
var openFile = function (event, idOut) {
    var input = event.target;
    var reader = new FileReader();
    reader.onload = function () {
        var dataURL = reader.result;
        var output = document.getElementById(idOut);
        output.src = dataURL;
    };
    reader.readAsDataURL(input.files[0]);
};
var field_name = null

function PopupCenter(url, title, w, h, fn) {
    var dualScreenLeft = window.screenLeft != undefined ? window.screenLeft : screen.left;
    var dualScreenTop = window.screenTop != undefined ? window.screenTop : screen.top;

    var width = window.innerWidth ? window.innerWidth : document.documentElement.clientWidth ? document.documentElement.clientWidth : screen.width;
    var height = window.innerHeight ? window.innerHeight : document.documentElement.clientHeight ? document.documentElement.clientHeight : screen.height;

    var left = ((width / 2) - (w / 2)) + dualScreenLeft;
    var top = ((height / 2) - (h / 2)) + dualScreenTop;
    var newWindow = window.open(url, title, 'scrollbars=yes, width=' + w + ', height=' + h + ', top=' + top + ', left=' + left);

    if (window.focus) {
        newWindow.focus();
    }
    field_name = fn;
    newWindow.field_name = field_name;
}

if (typeof Dropzone !== 'undefined') {

    var reload_upload = true;

    if (typeof page_name_upload == 'undefined') {
        var page_name_upload = 'files';
    }

    $(document).ready(function () {

        var html_modal_upload = '<div class="ui modal" id="modal-upload">' +
            '<div class="header">' +
            'Upload Files' +
            '</div>' +
            '<div class="image content">' +
            '<div class="description">' +
            '<div class="content"></div>' +
            '<p align="center">ลากไฟล์หรือคลิกที่พื้นที่สีเทาเพื่ออัพโหลดไฟล์</p>' +
            '<div class="dropzone" id="dropzoneSection" data-dz-message><span></span></div>' +
            '</div>' +
            '</div>' +
            '<div class="actions">' +
            '<div class="ui black deny button" onclick="closeModalUpload()">' +
            'ปิด' +
            '</div>' +
            '</div>' +
            '</div>';
        $('#modal-zone').append(html_modal_upload);

        Dropzone.autoDiscover = false;

        var option = {
            url: APP_URL + "admin/post/upload_file?page_name=" + page_name_upload,
            dictDefaultMessage: "",
            headers: {
                'X-CSRF-Token': APP_TOKEN
            }
        };

        var myDropzone = new Dropzone("div#dropzoneSection", option);

    });

    function removeAlbum(id) {
        var c = confirm('Confirm remove this album ?');
        if (!c) return false;
        $.post(APP_URL + 'admin/album/remove_album_member/' + id, {
            _token: APP_TOKEN
        }, function (res) {
            if (res.status == 100) {
                $('#album_member_' + id).remove();
            } else {
                alert("Can't remove this album. Please contact admin.");
            }
        });
    }

    function removeFile(id) {
        var c = confirm('ยืนยันการลบไฟล์ ?');
        if (!c) return false;
        $.post(APP_URL + 'admin/remove_file/' + id, {
            _token: APP_TOKEN
        }, function (res) {
            if (res.status === 100) {
                $('#file_id_' + id).remove();
            } else {
                alert("Can't remove this file. Please contact admin.");
            }
        });
    }

    function uploadTheFile(refresh) {

        if (!refresh) {
            reload_upload = false;
        }
        $('#modal-upload').modal('show');
    }

    function closeModalUpload() {
        if (reload_upload) {
            window.location.reload();
        } else {
            $('#modal-upload').modal('hide');
        }
    }
}

if (typeof size_md == 'undefined') {
    var size_md = '55';
}

function copyToClipboard(text) {
    var p = window.prompt("Ctrl+C for copy, Click ok for go to file", text);
    if (p) {
        window.location.href = text;
    }
}

var field_name = null

function PopupCenter(url, title, w, h, fn) {
    // Fixes dual-screen position                         Most browsers      Firefox
    var dualScreenLeft = window.screenLeft != undefined ? window.screenLeft : screen.left;
    var dualScreenTop = window.screenTop != undefined ? window.screenTop : screen.top;

    var width = window.innerWidth ? window.innerWidth : document.documentElement.clientWidth ? document.documentElement.clientWidth : screen.width;
    var height = window.innerHeight ? window.innerHeight : document.documentElement.clientHeight ? document.documentElement.clientHeight : screen.height;

    var left = ((width / 2) - (w / 2)) + dualScreenLeft;
    var top = ((height / 2) - (h / 2)) + dualScreenTop;
    var newWindow = window.open(url, title, 'scrollbars=yes, width=' + w + ', height=' + h + ', top=' + top + ', left=' + left);


    // Puts focus on the newWindow
    if (window.focus) {
        newWindow.focus();
    }

    field_name = fn;

    newWindow.field_name = field_name;
    //console.log(field_name);
}

var type_mode;
var page_old = 0;
var tool = null;
var where = null;
var multiple_files = false;

function filesSelect(type, page, tool_f, where_f, multiple_files_f) {
    if (!multiple_files_f) multiple_files_f = false;

    type_mode = type;
    tool = tool_f;
    where = where_f;
    multiple_files = multiple_files_f;

    if (page !== page_old) {
        $('.upload-picker-page-' + page_old).remove();
        page_old = page;
    }

    $.get(APP_URL + 'api/files_lists', {
        type: type,
        page: page
    }, function (res) {
        if (res) {
            var htmlModalFilePick = '<div class="ui long modal upload-picker-page-' + page + '" id="modal-upload-picker" >' +
                '<div class="header">' +
                'Files Pick' +
                '</div>' +
                '<div class="image content" >' +
                '<div class="content" ></div>' +
                '<div id="file_content_picker" style="margin-top: 10px;width: 100%;"></div>' +
                '</div>' +
                '<div class="actions">' +
                '<div class="ui black button" onclick="closeModalFilePick()">' +
                'ปิด' +
                '</div>' +
                '</div>' +
                '</div>';
            var insert = $('#modal-zone-pick').html(htmlModalFilePick);
            if (insert) {
                $('#file_content_picker').html(res);
                $('#modal-upload-picker').modal('show');
            }
        }
    });
}

function removeThumb(id) {
    $('#file_id_thumb_' + id).remove();
}

function removeCateProduct(id) {
    var c = confirm('Confirm remove this category product ?');
    if (!c) return false;
    $.post(APP_URL + 'admin/product/remove_cate/' + id, {
        _token: APP_TOKEN
    }, function (res) {
        if (res.status === 100) {
            $('#product_cate_' + id).remove();
        }
    });
}

function closeModalFilePick() {
    $('#modal-upload-picker').modal('hide').remove();
}

function addToPost(id, thumb, full_image) {
    closeModalFilePick();
    var timestamp = new Date().getTime();
    if (where && tool === null && multiple_files === false) {
        var html_thumbnail = '<div class="ui card" id="file_id_thumb_' + id + '">' +
            '<input type="hidden" name="id_files[]" value="' + id + '">' +
            '<input type="hidden" name="unique_id[]" value="' + timestamp + '">' +
            '<div class="image">' +
            '<div class="card-thumb" style="background: url(\'' + thumb + '\');"></div>' +
            '</div>' +
            '<div class="content">' +
            '<p align="center"><button type="button" onclick="removeThumb(' + id + ')" class="ui small icon red button"><i class="trash icon"></i></button></p>' +
            '</div>' +
            '</div>';
        $(where).html(html_thumbnail);
    }
    if (tool === 'textarea' && type_mode === 'files' && where) {
        //var content = tinyMCE.get(where).getContent();
        var html_link = '<a href="' + thumb + '" target="_blank">ไฟล์แนบ</a>';
        tinyMCE.get(where).execCommand('mceInsertContent', false, html_link);
    }
    if (tool === 'textarea' && type_mode === 'image' && where) {
        //var content = tinyMCE.get(where).getContent();
        var html_link = '<img src="' + full_image + '"/>';
        tinyMCE.get(where).execCommand('mceInsertContent', false, html_link);
    }
    $('.upload-picker-page-' + page_old).remove();
}