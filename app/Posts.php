<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Posts extends Model
{
    protected $table = 'posts';
    protected $primaryKey = 'id';
    protected $guarded = [];

    public function org()
    {
        return $this->hasOne('App\Cate', 'id', 'org_id');
    }

    public function cateToDetail()
    {
        return $this->hasMany('App\Cate', 'id', 'id_cate');
    }

    public function cate_relation()
    {
        return $this->hasMany('App\Cate_relation', 'id_post', 'id');
    }

    public function files()
    {
        return $this->hasOne('App\Files', 'id', 'id_file');
    }

    public function posts_quiz()
    {
        return $this->hasMany('App\PostsQuiz', 'id_post', 'id');
    }

    public function user()
    {
        return $this->hasOne('App\User', 'id', 'user_id');
    }

}
