<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class QuizResult extends Model
{
    protected $table = 'quiz_result';
    protected $guarded = [];

    public function post()
    {
        return $this->hasOne('App\Posts', 'id', 'id_quiz');
    }

    public function quiz_result_log()
    {
        return $this->hasMany('App\QuizResultLog', 'id_result', 'id');
    }
}
