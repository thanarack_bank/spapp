<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Cate_relation extends Model
{
    protected $table = 'cate_relation';
    protected $primaryKey = 'id_post';
    protected $guarded = [];
    public $timestamps = false;

    public function posts()
    {
        return $this->hasMany('App\Posts', 'id', 'id_post');
    }

    public function cate()
    {
        return $this->hasOne('App\Cate', 'id', 'id_cate');
    }
}
