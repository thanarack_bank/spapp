<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PostsQuiz extends Model
{
    protected $table = 'posts_quiz';
    protected $guarded = ['id'];

    public function answer_quiz()
    {
        return $this->hasMany('App\AnswerQuiz', 'id_quiz', 'id');
    }
}
