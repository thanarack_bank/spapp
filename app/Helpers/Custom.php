<?php

namespace App\Helpers;

use App\SettingsModel;
use Illuminate\Support\Facades\Storage;

class Custom
{
    public static function dateFix()
    {
        return 0;
    }

    public static function getSetting($key = null)
    {
        $setting = SettingsModel::where('keyrow', $key)->first();
        if (!$setting) return false;
        return $setting->value;
    }

}
