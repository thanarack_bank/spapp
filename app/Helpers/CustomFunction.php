<?php

if (!function_exists('urlGen')) {
    function urlGen($key = null, $value = null)
    {
        $url = $_GET;
        if ($key) unset($url[$key]);
        $url[$key] = $value;
        ksort($url);
        return request()->url() . '?' . http_build_query($url);
    }
}

if (!function_exists('getFileUrl')) {
    function getFileUrl($id_file)
    {
        $file = App\Files::find($id_file);
        $url_file = false;
        if ($file) $url_file = asset(env('URL_STORAGE') . '/' . $file->path_name . '/' . $file->full_name);
        return $url_file;
    }
}

if (!function_exists('fileConvertUrl')) {
    function fileConvertUrl($file, $thumb = false)
    {
        $url_file = asset('images/blank-file-3.png');
        $image_ext = ['jpg', 'png', 'gif', 'bmp', 'jpeg'];
        if (!empty($file)) {
            $full_path = asset(env('URL_STORAGE') . $file->path_name . '/' . $file->full_name);
            $thumb_path360 = $file->thumb_path360;
            if (in_array($file->ext, $image_ext)) {
                if ($thumb) {
                    if (!empty($thumb_path360)) {
                        $url_file = asset(env('URL_STORAGE') . $thumb_path360);
                    }
                } else {
                    $url_file = $full_path;
                }
            }
            return $url_file;
        }
        return $url_file;
    }
}

if (!function_exists('fileConvertUrlLink')) {
    function fileConvertUrlLink($file, $thumb = false)
    {
        //$url_file = asset('images/bb-gray.jpg');
        $url_file = asset('images/blank-file-3.png');
        $image_ext = ['jpg', 'png', 'gif', 'bmp', 'jpeg'];
        if (!empty($file)) {
            $full_path = $url_file = asset(env('URL_STORAGE') . $file->path_name . '/' . $file->full_name);
            $thumb_path360 = $file->thumb_path360;
            if (in_array($file->ext, $image_ext)) {
                if ($thumb) {
                    if (!empty($thumb_path360)) {
                        $url_file = asset(env('URL_STORAGE') . $thumb_path360);
                    }
                } else {
                    $url_file = $full_path;
                }
            } else {
                $url_file = $full_path;
            }
            return $url_file;
        }
        return $url_file;
    }
}

if (!function_exists('countryList')) {
    function countryList()
    {
        $countries = ["Afghanistan", "Albania", "Algeria", "American Samoa", "Andorra", "Angola", "Anguilla", "Antarctica", "Antigua and Barbuda", "Argentina", "Armenia", "Aruba", "Australia", "Austria", "Azerbaijan", "Bahamas", "Bahrain", "Bangladesh", "Barbados", "Belarus", "Belgium", "Belize", "Benin", "Bermuda", "Bhutan", "Bolivia", "Bosnia and Herzegowina", "Botswana", "Bouvet Island", "Brazil", "British Indian Ocean Territory", "Brunei Darussalam", "Bulgaria", "Burkina Faso", "Burundi", "Cambodia", "Cameroon", "Canada", "Cape Verde", "Cayman Islands", "Central African Republic", "Chad", "Chile", "China", "Christmas Island", "Cocos (Keeling) Islands", "Colombia", "Comoros", "Congo", "Congo, the Democratic Republic of the", "Cook Islands", "Costa Rica", "Cote d'Ivoire", "Croatia (Hrvatska)", "Cuba", "Cyprus", "Czech Republic", "Denmark", "Djibouti", "Dominica", "Dominican Republic", "East Timor", "Ecuador", "Egypt", "El Salvador", "Equatorial Guinea", "Eritrea", "Estonia", "Ethiopia", "Falkland Islands (Malvinas)", "Faroe Islands", "Fiji", "Finland", "France", "France Metropolitan", "French Guiana", "French Polynesia", "French Southern Territories", "Gabon", "Gambia", "Georgia", "Germany", "Ghana", "Gibraltar", "Greece", "Greenland", "Grenada", "Guadeloupe", "Guam", "Guatemala", "Guinea", "Guinea-Bissau", "Guyana", "Haiti", "Heard and Mc Donald Islands", "Holy See (Vatican City State)", "Honduras", "Hong Kong", "Hungary", "Iceland", "India", "Indonesia", "Iran (Islamic Republic of)", "Iraq", "Ireland", "Israel", "Italy", "Jamaica", "Japan", "Jordan", "Kazakhstan", "Kenya", "Kiribati", "Korea, Democratic People's Republic of", "Korea, Republic of", "Kuwait", "Kyrgyzstan", "Lao, People's Democratic Republic", "Latvia", "Lebanon", "Lesotho", "Liberia", "Libyan Arab Jamahiriya", "Liechtenstein", "Lithuania", "Luxembourg", "Macau", "Macedonia, The Former Yugoslav Republic of", "Madagascar", "Malawi", "Malaysia", "Maldives", "Mali", "Malta", "Marshall Islands", "Martinique", "Mauritania", "Mauritius", "Mayotte", "Mexico", "Micronesia, Federated States of", "Moldova, Republic of", "Monaco", "Mongolia", "Montserrat", "Morocco", "Mozambique", "Myanmar", "Namibia", "Nauru", "Nepal", "Netherlands", "Netherlands Antilles", "New Caledonia", "New Zealand", "Nicaragua", "Niger", "Nigeria", "Niue", "Norfolk Island", "Northern Mariana Islands", "Norway", "Oman", "Pakistan", "Palau", "Panama", "Papua New Guinea", "Paraguay", "Peru", "Philippines", "Pitcairn", "Poland", "Portugal", "Puerto Rico", "Qatar", "Reunion", "Romania", "Russian Federation", "Rwanda", "Saint Kitts and Nevis", "Saint Lucia", "Saint Vincent and the Grenadines", "Samoa", "San Marino", "Sao Tome and Principe", "Saudi Arabia", "Senegal", "Seychelles", "Sierra Leone", "Singapore", "Slovakia (Slovak Republic)", "Slovenia", "Solomon Islands", "Somalia", "South Africa", "South Georgia and the South Sandwich Islands", "Spain", "Sri Lanka", "St. Helena", "St. Pierre and Miquelon", "Sudan", "Suriname", "Svalbard and Jan Mayen Islands", "Swaziland", "Sweden", "Switzerland", "Syrian Arab Republic", "Taiwan, Province of China", "Tajikistan", "Tanzania, United Republic of", "Thailand", "Togo", "Tokelau", "Tonga", "Trinidad and Tobago", "Tunisia", "Turkey", "Turkmenistan", "Turks and Caicos Islands", "Tuvalu", "Uganda", "Ukraine", "United Arab Emirates", "United Kingdom", "United States", "United States Minor Outlying Islands", "Uruguay", "Uzbekistan", "Vanuatu", "Venezuela", "Vietnam", "Virgin Islands (British)", "Virgin Islands (U.S.)", "Wallis and Futuna Islands", "Western Sahara", "Yemen", "Yugoslavia", "Zambia", "Zimbabwe"];
        return $countries;
    }
}

if (!function_exists('formatIDUser')) {
    function formatIDUser($id)
    {
        $id = sprintf('#%010d', $id);
        return $id;
    }
}

if (!function_exists('formatIDAlbum')) {
    function formatIDAlbum($id)
    {
        $id = sprintf('#%010d', $id);
        return $id;
    }
}

if (!function_exists('imagesFormat')) {
    function imagesFormat()
    {
        $images = ['jpg', 'png', 'gif', 'jpeg', 'bmp'];
        return $images;
    }
}

if (!function_exists('productType')) {
    function productType($type)
    {
        $typeName = null;
        if ($type == 'i') {
            $typeName = 'Single photo';
        } else {
            $typeName = 'Group photo';
        }
        return $typeName;
    }
}

if (!function_exists('format_uri_slug')) {
    function format_uri_slug($str)
    {
        $str = strtolower(trim($str));
        $str = preg_replace('/[^a-z0-9-ก-๙เแ]/', '-', $str);
        $str = preg_replace('/-+/', "-", $str);
        return $str;
    }
}

function getListCate($type = 'category')
{
    $cate = \App\Cate::where(['type' => $type, 'active' => 'yes'])->get();
    return $cate;
}

function ago_time($time)
{
    $timediff = time() - $time;
    $days = intval($timediff / 86400);
    $remain = $timediff % 86400;
    $hours = intval($remain / 3600);
    $remain = $remain % 3600;
    $mins = intval($remain / 60);
    $secs = $remain % 60;

    if ($secs >= 0) $timestring = "0m" . $secs . "s";
    if ($mins > 0) $timestring = $mins . "m" . $secs . "s";
    if ($hours > 0) $timestring = $hours . "u" . $mins . "m";
    if ($days > 0) $timestring = $days . "d" . $hours . "u";

    return $timestring;
}


if (!function_exists('config_db')) {
    function config_db($key)
    {
        $config = \App\SettingsModel::where([
            'keyrow' => $key
        ])->first();
        if (!$config) return false;
        return $config->value;
    }
}


function make_url_page($page)
{
    $cul_url = $_GET;
    $cul_url['page'] = $page;
    $new = \Illuminate\Support\Facades\Request::url() . '?' . http_build_query($cul_url);
    return $new;
}

function getMyPage()
{
    $url = \Illuminate\Support\Facades\Request::fullUrl();
    return ($url);
}

function getMySegmentPage($url)
{
    $my_url = env('APP_URL');
    $url = str_replace($my_url, null, $url);
    return $url;
}

function store_path()
{
    return public_path(env('URL_STORAGE'));
}

function truncate($text, $length = 400)
{
    $text = html_entity_decode($text);
    $text = trim(preg_replace('/\s\s+/', ' ', $text));
    $text = strip_tags($text);
    $text = iconv_substr($text, 0, $length, 'UTF-8');
    return ($text);
}