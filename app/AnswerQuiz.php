<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class AnswerQuiz extends Model
{
    protected $table = 'answer_quiz';
    protected $guarded = ['id'];
}
