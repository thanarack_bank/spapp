<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class QuizResultLog extends Model
{
    protected $table = 'quiz_result_log';
    protected $guarded = [];
    public $timestamps = false;
}
