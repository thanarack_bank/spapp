<?php

namespace App\Http\Middleware;

use App\Cate;
use Closure;

class FrontEnd
{
    public function handle($request, Closure $next)
    {
        $request->allCategory = Cate::where(['active' => 'yes', 'type' => 'category'])->get();
        return $next($request);
    }
}
