<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Support\Facades\Redirect;

class AdminAuth
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  \Closure $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if (!empty($request->user()) && $request->user()->user_info->position == 'admin') {
            return $next($request);
        }
        // abort('500', 'สำหรับผู้ดูแลเท่านั้น');
        return Redirect::to('/');
    }
}
