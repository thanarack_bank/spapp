<?php

namespace App\Http;

use App\Files;
use App\Helpers\Custom;
use Image;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Storage;

class FilesApp
{
    public $fileAllow = 'image';
    public $pageName = 'files';
    protected $allowExtImage = ['png', 'jpg', 'gif', 'jpeg'];
    protected $maxFileSize = 102400;

    public function __construct()
    {
        config(['filesystems.disks.public.root' => store_path()]);
    }

    public function uploadToServer($file, $post = true, $key = 'file')
    {
        if ($this->fileAllow == 'image') {
            $rule = [$key => 'image'];
            $validator = Validator::make($file, $rule, ['image' => 'กรุณาอัพโหลดรูปภาพเท่านั้น']);

            if ($validator->fails()) {
                return $validator->error = $validator;
            }
        }

        $thumbName360 = null;
        $thumbName768 = null;
        $fullPathThumb360 = null;
        $fullPathThumb768 = null;

        $fileData = $file[$key];
        $name = $fileData->getClientOriginalName();
        $path = date('Y') . '/' . date('m');
        $ext = explode('.', $name);
        $ext = strtolower(end($ext));
        $rename = md5($name . uniqid());
        $fullPath = $path . '/' . $rename . '.' . $ext;
        $size = $fileData->getSize();
        $file = file_get_contents($fileData->getRealPath());
        $fullName = $rename . '.' . $ext;
        $upload = Storage::disk('public')->put($fullPath, $file);
        if ($upload) {
            if (in_array($ext, $this->allowExtImage)) {
                $thumbName360 = $rename . '-360thumb.' . $ext;
                $thumbName768 = $rename . '-768thumb.' . $ext;
                $fullPathThumb360 = $path . '/' . $thumbName360;
                $fullPathThumb768 = $path . '/' . $thumbName768;

                Image::make($fileData->getRealPath())->resize('360', null, function ($constraint) {
                    $constraint->aspectRatio();
                })->save(store_path() . $fullPathThumb360);

                Image::make($fileData->getRealPath())->resize('768', null, function ($constraint) {
                    $constraint->aspectRatio();
                })->save(store_path() . $fullPathThumb768);
            }

            $res = [
                'name' => $rename,
                'oldName' => $name,
                'path' => $path,
                'size' => $size,
                'fullName' => $fullName,
                'thumbName360' => $thumbName360,
                'thumbPath360' => $fullPathThumb360,
                'thumbName760' => $thumbName768,
                'thumbPath760' => $fullPathThumb768,
                'fullPath' => $fullPath,
                'urlPublish' => env('APP_URL') . env('URL_STORAGE') . $fullPath,
                'urlPublishThumb' => isset($fullPathThumb) ? env('APP_URL') . env('URL_STORAGE') . $fullPathThumb : null,
                'ext' => $ext
            ];
            $res['file'] = $this->saveFileToDb($res);
            return $res;
        } else {
            abort(500, 'ไม่สามารถอัพโหลดไฟ์ได้ กรุณาติดต่อ Admin');
        }
    }

    private function saveFileToDb($data)
    {
        $files = new Files();
        $files->id_user = Auth::id();
        $files->name = $data['name'];
        $files->old_name = $data['oldName'];
        $files->full_name = $data['fullName'];
        $files->page_name = $this->pageName;
        $files->path_name = $data['path'];
        $files->ext = $data['ext'];
        $files->thumb360 = $data['thumbName360'];
        $files->thumb_path360 = $data['thumbPath360'];
        $files->thumb768 = $data['thumbName760'];
        $files->thumb_path768 = $data['thumbPath760'];
        $files->size = $data['size'];
        $files->save();
        return $files;
    }

    public function removeFile($id)
    {
        $File = Files::find($id);
        if ($File) {
            Storage::delete($File->path_name . '/' . $File->full_name);
            if (in_array($File->ext, $this->allowExtImage)) {
                Storage::delete([$File->thumb_path360, $File->thumb_path768]);
            }
            $File->delete();
            return true;
        }
        return false;
    }

}
