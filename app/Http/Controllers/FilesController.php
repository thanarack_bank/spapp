<?php

namespace App\Http\Controllers;

use App\Album;
use App\AlbumFiles;
use App\AlbumRelation;
use App\Files;
use Illuminate\Support\Facades\Storage;
use stdClass;
use Illuminate\Http\Request;

class FilesController extends Controller
{
    protected $allowExtImage = ['png', 'jpg', 'gif', 'jpeg'];

    public function __construct()
    {
        config(['filesystems.disks.public.root' => store_path()]);
    }

    public function index(Request $request)
    {
        $data = new stdClass();
        $data->list_files = Files::where([
            'id_user' => $request->user()->id,
            'public' => 'yes'
        ])
            ->orderBy('id', 'desc')
            ->paginate(20);
        return view('admin.files', ['data' => $data]);
    }

    public function files_lists(Request $request)
    {
        if (!$request->ajax()) abort(505, 'Request ajax only !');

        $data = new stdClass();
        $type = $request->input('type');

        $condition = [
            'id_user' => $request->user()->id,
            'public' => 'yes'
        ];

        if ($type && $type != 'image') {
            $condition['page_name'] = $request->input('type');
        }

        if ($type == 'image') {
            $data->list_files = Files::where($condition)
                ->whereIn('ext', ['jpg', 'png', 'jpeg', 'gif'])
                ->orderBy('id', 'desc')
                ->paginate(15);
        } else {
            $data->list_files = Files::where($condition)
                ->orderBy('id', 'desc')
                ->paginate(15);
        }

        return view('admin.files_lists_json', ['data' => $data]);
    }

    public function remove_file(Request $request, $id)
    {
        $File = Files::where([
            'id' => $id,
            'id_user' => $request->user()->id,
            'public' => 'yes'
        ])->first();
        if ($File) {

            //remove file
            Storage::delete($File->path_name . '/' . $File->full_name);

            //remove thumb file
            if (in_array($File->ext, $this->allowExtImage)) {
                Storage::delete([$File->thumb_path360, $File->thumb_path768]);
            }

            //remove on database
            $File->delete();

            return response()->json([
                'status' => 100
            ]);
        } else {
            return response()->json([
                'status' => 200
            ]);
        }
    }

}
