<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class LoginController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    use AuthenticatesUsers;

    /**
     * Where to redirect users after login.
     *
     * @var string
     */
    protected function redirectTo()
    {
        $red = request()->session()->get('red');

        if (request()->user()->active == 'no') {
            Auth::logout();
            request()->session()->flash('user-ban', true);
            return 'login';
        }

        if ($red) {
            request()->session()->remove('red');
            return getMySegmentPage($red);
        }

        if (request()->user()->user_info->position == 'admin') {
            return 'admin/dashboard';
        }

        return '/';
    }

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest')->except('logout');
    }

    public function showLoginForm(Request $request)
    {
        $red = $request->input('red');
        if ($red) $request->session()->put('red', $red);
        return view('auth.login');
    }

}
