<?php

namespace App\Http\Controllers;

use App\Cate;
use App\Cate_relation;
use App\Posts;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Redirect;

class CateController extends Controller
{
    public function all_page(Request $request)
    {
        // Set class data
        $data = new \stdClass();

        // Loading data
        $data->last_post = Posts::where(['active' => 'publish', 'post_type' => 'post'])
            ->orderBy('updated_at', 'desc')
            ->paginate(12);

        // Render
        return view('all', ['data' => $data]);
    }

    public function view_tag(Request $request, $type = 'tag', $id)
    {
        // Convert input
        $id = htmlspecialchars($id);

        // check type
        $allowTag = ['category', 'tag', 'org'];
        if (!in_array($type, $allowTag)) return Redirect::to('/');

        // Find the category
        $data = Cate::where('type', '=', $type)->where('slug', $id)->first();
        if (!$data) return redirect()->to('/');

        // Set type of category
        $data->type_txt = $this->type_cate($data->type);

        // Loading data
        $data->last_post = Posts::join('cate_relation', 'cate_relation.id_post', 'posts.id')
            ->where('posts.active', 'publish')
            ->where('cate_relation.id_cate', $data->id)
            ->orderBy('posts.updated_at', 'desc')
            ->paginate(20);

        // Render
        return view('view_tag', ['data' => $data]);
    }

    public function search(Request $request)
    {
        $data = new \stdClass();
        $data->title = 'ผลการค้นหา: ' . $request->input('q');
        return view('search', ['data' => $data]);
    }

    private function type_cate($type)
    {
        $txt = null;
        switch ($type) {
            case 'tag':
                $txt = 'Tag';
                break;
            case 'category':
                $txt = 'หมวดหมู่';
                break;
            case 'org':
                $txt = 'หน่วยงาน';
                break;
                break;
        }
        return $txt;
    }


}
