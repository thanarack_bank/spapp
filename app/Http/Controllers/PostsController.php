<?php

namespace App\Http\Controllers;

use App\Cate;
use App\Cate_relation;
use App\Posts;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Redirect;

class PostsController extends Controller
{
    public function view_post(Request $request, $id)
    {

        // Load post
        $data = Posts::where(['id' => $id, 'active' => 'publish', 'post_type' => 'post'])->first();

        // Check exit post
        if (!$data) return Redirect::to('/');

        // Tags
        //$tags = $data->cate_relation()->join('cate', 'cate_relation.id_cate', 'cate.id')->get();
        /*$tags = Cate_relation::where([
            'id_post' => $id
        ])->get();*/
        $tags = Cate_relation::join('cate', 'cate.id', 'cate_relation.id_cate')->where(['cate_relation.id_post' => $id, 'cate.type' => 'tag'])->get();

        //Count View Post
        $data->views = $data->views + 1;
        $data->save();

        // Load relation post
        $data->relate_post = Posts::where(['active' => 'publish', 'post_type' => 'post'])->where('id', '!=', $id)->orderBy('id', 'desc')->limit(4)->get();

        // Set meta
        $data->des = truncate($data->content);

        // Render
        return view('view_post', ['data' => $data, 'tags' => $tags]);
    }

    private function find_tag()
    {
        return 0;
    }
}
