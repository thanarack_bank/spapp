<?php

namespace App\Http\Controllers\Admin;

use App\Cate;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Redirect;

class CateController extends Controller
{
    public function manger(Request $request, $name = null)
    {
        $nameAllow = ['org', 'menu', 'tag', 'quiz'];
        if (!in_array($name, $nameAllow)) return redirect()->to('/');

        $data = new \stdClass();

        switch ($name) {
            case 'org':
                $data->title = 'หน่วยงาน';
                break;
            case 'menu':
                $name = 'category';
                $data->title = 'เมนู';
                break;
            case  'tag':
                $data->title = 'แท็ค';
                break;
            case  'quiz':
                $data->title = 'ข้อสอบ';
                break;
        }

        $data->list_category = Cate::where([
            'type' => $name
        ])
            ->orderBy('id', 'desc')
            ->paginate(20);

        return view('admin.list_cate', ['data' => $data]);
    }

    public function category_edit(Request $request)
    {
        $id_update = $request->input('id');
        $data = new \stdClass();
        if ($id_update) {
            $data->post = Cate::find($id_update);
            if (!$data->post) return redirect()->to('admin/dashboard');
        }
        return view('admin.category_edit', ['data' => $data]);
    }

    public function category_edit_save(Request $request)
    {
        $id_update = $request->input('id_update');
        $go = $request->input('type');
        if ($id_update) {
            $category = Cate::find($id_update);
        } else {
            $category = new Cate();
        }

        $category->title = $request->input('title');
        $category->slug = format_uri_slug(htmlspecialchars($request->input('slug')));
        $category->type = $request->input('type');
        $category->active = $request->input('active');
        $category->save();

        if ($request->input('type') == 'category') {
            $go = 'menu';
        }

        return Redirect::to('admin/category/' . $go)->with('success-save', true);
    }


    public function remove_cate(Request $request, $id)
    {
        if (!$request->ajax()) return redirect()->to('/');
        $post = Cate::find($id)->delete();
        if ($post) {
            return response()->json([
                'status' => 100
            ]);
        }
    }
}
