<?php

namespace App\Http\Controllers\Admin;

use App\Cate;
use App\Cate_relation;
use App\Files;
use App\Http\FilesApp;
use App\Posts;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Redirect;

class Post extends Controller
{
    public function new_post(Request $request)
    {
        $id_post = $request->input('id');
        $data = new \stdClass();
        // Load cate
        $data->list_org = Cate::where('active', 'yes')->where('type', 'org')->get();
        $data->list_category = Cate::where('active', 'yes')->where('type', 'category')->get();

        if ($id_post) {
            $data->post = Posts::where(['id' => $id_post, 'post_type' => 'post'])->first();
            if (!$data->post) return redirect()->to('admin/post/list');
            $data->list_tags = [];
            foreach ($data->post->cate_relation->toArray() as $value) {
                $data->list_tags[$value['id_cate']] = $value['id_cate'];
            }
            $list_tags2 = Cate::where(['type' => 'tag'])->whereIn('id', $data->list_tags)->get()->toArray();
            $data->list_tags2 = [];
            foreach ($list_tags2 as $key => $value) {
                $data->list_tags2[] = $value['slug'];
            }
        }

        return view('admin/new_post', ['data' => $data]);
    }


    public function new_page(Request $request, $slug = null)
    {
        $data = new \stdClass();
        $data->post = Posts::where(['slug' => $slug])->first();
        if (!$data->post) return redirect()->to('admin/dashboard');
        return view('admin/new_page', ['data' => $data]);
    }

    public function save_page(Request $request)
    {
        $id_update = (int)$request->input('id_update');
        $post = Posts::find($id_update);

        if ($post) {
            $post->title = mb_substr($request->input('title'), 0, 300, "utf-8");
            $post->content = htmlspecialchars($request->input('content'));
            $post->user_id = 0;
            $post->org_id = 0;
            $post->active = 'publish';
            $post->id_file = 0;
            $post->views = 0;
            $post->save();
        }

        return Redirect::to('admin/page/' . $post->slug)->with('success-save', true);
    }

    public function list_post(Request $request)
    {
        $data = new  \stdClass();
        $data->list_post = Posts::where(['post_type' => 'post'])->paginate(20);
        return view('admin/list_post', ['data' => $data]);
    }

    public function remove_post(Request $request, $id)
    {
        if (!$request->ajax()) return redirect()->to('/');
        $post = Posts::find($id)->delete();
        if ($post) {
            Cate_relation::where([
                'id_post' => $id
            ])->delete();
            return response()->json([
                'status' => 100
            ]);
        }
    }

    public function save_post(Request $request)
    {

        //dd($request->input());
        $id_update = (int)$request->input('id_update');
        $tags = $request->input('tag') ? explode(',', $request->input('tag')) : null;
        $category = $request->input('category') ? $request->input('category') : null;

        if ($request->input('id_files')) {
            $id_file = $request->input('id_files');
        } else {
            $id_file = [];
        }

        if ($id_update) {
            $post = Posts::find($id_update);
            $id_use_cate = [];
        } else {
            $post = new Posts();
        }

        $post->title = mb_substr($request->input('title'), 0, 300, "utf-8");
        $post->slug = mb_substr(format_uri_slug($request->input('title')), 0, 150, "utf-8").'-'.rand();
        $post->content = htmlspecialchars($request->input('content'));
        $post->user_id = $request->user()->id;
        $post->org_id = $request->input('org');
        $post->active = $request->input('active');
        $post->id_file = (isset($id_file[0])) ? $id_file[0] : null;
        $post->views = 0;
        $post->save();

        if ($post && $tags) {
            foreach ($tags as $key => $value) {
                $id_cate = $this->addCate('tag', $value, $post->id);
                if ($id_update) array_push($id_use_cate, $id_cate);
            }
        }

        if ($post && $category) {
            foreach ($category as $key => $value) {
                $id_cate = $this->addCate('category', $value, $post->id);
                if ($id_update) array_push($id_use_cate, $id_cate);
            }
        }

        if ($id_update) {
            Cate_relation::where(['id_post' => $id_update])->whereNotIn('id_cate', $id_use_cate)->delete();
        }

        return Redirect::to('admin/post/list')->with('success-save', true);
    }

    public function addCate($type = 'tag', $value, $id_post = null)
    {
        $checkExitC = Cate::where(['slug' => $value, 'type' => $type])->first();
        if (!$checkExitC) {
            $createCate = Cate::create([
                'title' => $value,
                'slug' => format_uri_slug($value),
                'active' => 'yes',
                'type' => $type,
                'sort' => 0
            ]);
            $id_cate = $createCate->id;
        } else {
            $id_cate = $checkExitC->id;
        }

        if ($id_post && $id_cate) {
            $checkExitCT = Cate_relation::where(['id_post' => $id_post, 'id_cate' => $id_cate])->first();
            if ($checkExitCT) {
                $checkExitCT->id_cate = $id_cate;
                $checkExitCT->save();
            } else {
                Cate_relation::create([
                    'id_post' => $id_post,
                    'id_cate' => $id_cate
                ]);
            }
        }

        return $id_cate;
    }

    public function uploadFile(Request $request)
    {
        $page_name = $request->input('page_name');
        if (isset($_FILES['file'])) {
            $fileApp = new FilesApp();
            $fileApp->fileAllow = '*';
            if ($page_name) $fileApp->pageName = $page_name;
            $uploadFile = $fileApp->uploadToServer(Input::file());
            if (!isset($uploadFile->error)) {
                return view('admin/uploadDataSuccess', ['uploadFile' => $uploadFile]);
            }
        }
        return view('admin/uploadData')->withErrors(isset($uploadFile->error) ? $uploadFile->error : null);
    }

    /*public function files_lists(Request $request)
    {
        if (!$request->ajax()) return redirect()->to('/');
        $files = Files::orderBy('id', 'desc')->paginate(15);
        return view('admin/files_lists_json', ['files' => $files]);
    }*/
}
