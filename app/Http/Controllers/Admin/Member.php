<?php

namespace App\Http\Controllers\Admin;

use App\User;
use App\User_info;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;

class Member extends Controller
{
    public function index(Request $request)
    {
        $data = new  \stdClass();

        $data->list_member = DB::table('users')
            ->join('users_info', 'users_info.id_user', '=', 'users.id')
            ->where(['users_info.position' => 'user'])
            ->orderBy('users.id', 'desc')
            ->paginate(20);

        return view('admin/member-list', ['data' => $data]);
    }

    public function remove_member(Request $request, $id)
    {
        if (!$request->ajax()) return redirect()->to('/');
        $member = User::find($id)->delete();
        if ($member) {
            User_info::where([
                'id_user' => $id
            ])->delete();
            return response()->json([
                'status' => 100
            ]);
        }
    }

    public function member_ban(Request $request, $id)
    {
        if (!$request->ajax()) return redirect()->to('/');
        $active = $request->input('active');
        $member = User::find((int)$id);
        if ($member) {
            $member->active = $active == 'yes' ? 'no' : 'yes';
            $member->save();
            return response()->json([
                'status' => 100
            ]);
        }
    }

}
