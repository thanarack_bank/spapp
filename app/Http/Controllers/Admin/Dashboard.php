<?php

namespace App\Http\Controllers\Admin;

use App\Posts;
use App\PostsQuiz;
use App\User;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class Dashboard extends Controller
{
    public function index()
    {
        $data = new \stdClass();
        $data->member = User::count();
        $data->posts = Posts::where(['active' => 'publish', 'post_type' => 'post'])->count();
        $data->quiz = Posts::where(['active' => 'publish', 'post_type' => 'quiz'])->count();
        return view('admin/dashboard', ['data' => $data]);
    }
}
