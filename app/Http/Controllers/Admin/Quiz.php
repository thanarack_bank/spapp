<?php

namespace App\Http\Controllers\Admin;

use App\AnswerQuiz;
use App\Cate;
use App\Cate_relation;
use App\Posts;
use App\PostsQuiz;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Auth;

class Quiz extends Controller
{
    public function index()
    {
        // Set class data
        $data = new \stdClass();
        $data->list_post = Posts::where(['post_type' => 'quiz'])->orderBy('id','desc')->paginate(20);
        // Render
        return view('admin.quiz-list', ['data' => $data]);
    }

    public function add_quiz(Request $request)
    {
        if (!Auth::check()) return redirect()->to('/');
        $id_post = $request->input('id');
        // Set class data
        $data = new \stdClass();
        //$data->id_run = session('id_run', md5(time().uniqid()));
        $data->list_category = Cate::where(['type' => 'quiz'])->get();

        if ($id_post) {
            $data->post = Posts::where(['id' => $id_post, 'post_type' => 'quiz'])->first();
            if (!$data->post) return redirect()->to('admin/quiz/list');
            $data->list_tags = [];
            foreach ($data->post->cate_relation->toArray() as $value) {
                $data->list_tags[$value['id_cate']] = $value['id_cate'];
            }
            $data->list_question = PostsQuiz::where(['id_post' => $id_post])->get();
        }

        // Render
        return view('admin.quiz-add', ['data' => $data]);
    }

    public function add_quiz_save(Request $request)
    {
        if (!Auth::check()) return redirect()->to('/');
        //dd($request->input());
        $id_update = $request->input('id_update');
        $question = $request->input('question');
        $category = !empty($request->input('category')) ? $request->input('category') : null;
        if ($request->input('id_files')) {
            $id_file = $request->input('id_files');
        } else {
            $id_file = [];
        }

        if ($id_update) {
            $id_use_cate = [];
            $post = Posts::find($id_update);
            $list_quiz = PostsQuiz::where(['id_post' => $id_update])->get();
            $list_quiz_array = $list_quiz->toArray();
            $id_qt = $request->input('id_quiz_edit');
        } else {
            $post = new Posts();
        }

        $post->title = mb_substr($request->input('title'),0,300, "utf-8");
        $post->slug = mb_substr(format_uri_slug($request->input('title')),0,150, "utf-8").'-'.rand();
        $post->content = strip_tags($request->input('content'));
        $post->user_id = $request->user()->id;
        $post->active = $request->input('active');
        $post->id_file = (isset($id_file[0])) ? $id_file[0] : null;
        $post->post_type = 'quiz';
        $post->save();

        if (!empty($question) && $id_update && $list_quiz) {
            $result_delete = PostsQuiz::where(['id_post' => $id_update])->whereNotIn('id', $id_qt)->delete();
            if ($result_delete) {
                foreach ($list_quiz_array as $value) {
                    if (!in_array($value['id'], $id_qt)) {
                        AnswerQuiz::where(['id_quiz' => $value['id']])->delete();
                    }
                }
            }
        }

        if (!empty($question)) {
            foreach ($question as $key_question => $value_question) {

                $real_id_question = $key_question + 1;
                $choice_answer = $request->input('answer_true_' . $real_id_question);
                $answer_question = $request->input('answer_' . $real_id_question);

                if (isset($list_quiz_array[$key_question]) && $list_quiz_array[$key_question]['id'] == $id_qt[$key_question]) {
                    $id_update_post = $id_qt[$key_question];
                    $id_update_answer = $request->input('id_quiz_answer_' . $id_update_post);
                    $update_post_quiz = PostsQuiz::find($id_update_post);
                    $update_post_quiz->title = strip_tags($value_question);
                    $update_post_quiz->save();
                    if ($choice_answer) {
                        PostsQuiz::where('id', $id_update_post)->update([
                            'id_answer' => $choice_answer
                        ]);
                        foreach ($answer_question as $key_answer => $value_answer) {
                            $insert_answer = AnswerQuiz::find($id_update_answer[$key_answer]);
                            if ($insert_answer) {
                                $insert_answer->title = strip_tags($value_answer);
                                $insert_answer->save();
                            }
                        }
                    }
                } else {
                    $insert_post_quiz = PostsQuiz::create([
                        'id_post' => $post->id,
                        'id_answer' => 0,
                        'title' => $value_question
                    ]);
                    if ($insert_post_quiz) {
                        foreach ($answer_question as $key_answer => $value_answer) {
                            $real_id_answer = $key_answer + 1;
                            $insert_answer = AnswerQuiz::create([
                                'id_quiz' => $insert_post_quiz->id,
                                'title' => ($value_answer) ? $value_answer : 'No title'
                            ]);
                            if ($real_id_answer == $choice_answer) {
                                PostsQuiz::where('id', $insert_post_quiz->id)->update([
                                    'id_answer' => $insert_answer->id
                                ]);
                            }
                        }
                    }

                }
            }
        }

        if ($post && $category) {
            foreach ($category as $key => $value) {
                $PostAdmin = new Post();
                $id_cate = $PostAdmin->addCate('quiz', $value, $post->id);
                if ($id_update && $id_cate) array_push($id_use_cate, $id_cate);
            }
        }

        if ($id_update) {
            Cate_relation::where(['id_post' => $id_update])->whereNotIn('id_cate', $id_use_cate)->delete();
        }

        return Redirect::to('admin/quiz/list')->with('success-save', true);
    }


}
