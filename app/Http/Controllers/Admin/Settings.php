<?php

namespace App\Http\Controllers\Admin;

use App\Http\FilesApp;
use App\SettingsModel;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Input;

class Settings extends Controller
{
    public function index()
    {
        $data = new \stdClass();
        $data->setting = SettingsModel::all();

        $setting = [];
        foreach ($data->setting as $key => $value) {
            $setting[$value->keyrow] = $value->value;
        }

        return view('admin/settings', ['data' => $data, 'setting' => $setting]);
    }

    public function social()
    {
        $data = new \stdClass();
        $data->setting = SettingsModel::all();
        $setting = [];
        foreach ($data->setting as $key => $value) {
            $setting[$value->keyrow] = $value->value;
        }
        return view('admin/settings-social', ['data' => $data, 'setting' => $setting]);
    }

    public function ads()
    {
        $data = new \stdClass();
        $data->setting = SettingsModel::all();
        $setting = [];
        foreach ($data->setting as $key => $value) {
            $setting[$value->keyrow] = $value->value;
        }
        return view('admin/settings-ads', ['data' => $data, 'setting' => $setting]);
    }

    public function save_settings(Request $request)
    {
        $fileApp = new FilesApp();
        foreach ($request->all() as $key => $value) {
            if ($key != '_token') {
                $checkExitKey = SettingsModel::where('keyrow', $key)->first();

                if (isset($_FILES[$key])) {
                    $uploadIcon = $fileApp->uploadToServer(Input::file(), false, $key);
                    if (isset($uploadIcon->error)) return redirect()->back()->withErrors($uploadIcon->error->errors()->all());
                    $value = $uploadIcon['urlPublish'];
                }

                if (!$checkExitKey) {
                    SettingsModel::create([
                        'keyrow' => $key,
                        'value' => $value
                    ]);
                } else {
                    $checkExitKey->value = $value;
                    $checkExitKey->save();
                }
            }
        }
        return back()->with('success-save', true);
    }
}
