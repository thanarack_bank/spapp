<?php

namespace App\Http\Controllers;

use App\Cate;
use App\Http\Controllers\Admin\Quiz;
use App\Posts;
use App\PostsQuiz;
use App\QuizResult;
use App\QuizResultLog;
use App\User;
use App\User_info;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

class QuizController extends Controller
{
    public function index(Request $request)
    {
        $data = new \stdClass();
        // Set meta
        $data->title = config_db('quiz_title');
        $data->des = config_db('quiz_title');

        $data->last_post = Posts::where(['active' => 'publish', 'post_type' => 'quiz'])
            ->limit(6)
            ->orderBy('updated_at', 'asc')
            ->get();

        $data->last_post_quiz = Cate::join('cate_relation', 'cate_relation.id_cate', '=', 'cate.id')
            ->select('cate.id', DB::raw('count("cate_relation.id_cate") as total'))
            ->where(['cate.type' => 'quiz'])
            ->groupBy('cate.id')
            ->orderBy('total', 'desc')
            ->limit(3)
            ->get();

        foreach ($data->last_post_quiz as $key => $value) {
            $data->cate_detail[$key] = Cate::find($value->id);
            $data->quiz_menu[$key] = Posts::join('cate_relation', 'cate_relation.id_post', '=', 'posts.id')
                ->where(['posts.active' => 'publish', 'posts.post_type' => 'quiz', 'cate_relation.id_cate' => $value->id])
                ->limit(6)
                ->orderBy('posts.updated_at', 'asc')
                ->get();
        }

        return view('quiz/index', ['data' => $data]);
    }

    public function index_all(Request $request){
        $data = new \stdClass();
        // Order
        $order = $request->input('order');
        if($order == '1'){
            $order = 'desc';
        } else if($order == '2'){
            $order = 'asc';
        } else {
            $order = 'asc';
        }

        // Set meta
        $data->title = 'รายการแบบทดสอบทั้งหมด';
        $data->last_post = Posts::where(['active' => 'publish', 'post_type' => 'quiz'])
            ->orderBy('updated_at', $order)
            ->paginate(20);
        return view('quiz/index_all', ['data' => $data]);
    }

    public function category(Request $request, $slug = null)
    {
        $data = new \stdClass();
        $data->cate = Cate::where(['slug' => $slug, 'type'=>'quiz'])->first();
        if (!$data->cate) return redirect()->to('/');
        $data->last_post = Posts::join('cate_relation', 'cate_relation.id_post', '=', 'posts.id')
            ->where(['posts.active' => 'publish', 'posts.post_type' => 'quiz', 'cate_relation.id_cate' => $data->cate->id])
            ->orderBy('posts.updated_at', 'asc')
            ->paginate(20);
        $data->title = $data->cate->title;

        return view('quiz/category', ['data' => $data]);
    }

    public function play(Request $request, $id)
    {
        $data = new \stdClass();
        $data->post = Posts::find($id);
        if (!$data->post) return redirect()->to('/');
        $data->title = 'แบบทดสอบ ' . $data->post->title;
        $data->des = $data->post->content;
        $data->list_question = PostsQuiz::where(['id_post' => $id])->get();

        //Update view
        $data->post->views = $data->post->views + 1;
        $data->post->save();
        return view('quiz/play', ['data' => $data]);
    }

    public function save_play(Request $request, $id)
    {
        //dd($request->input());
        $id_key = md5(time() . uniqid());
        $data = new QuizResult();
        $data->id_key = $id_key;
        $data->id_quiz = $id;
        $data->id_user = $request->user()->id;
        $data->score = 0;
        $data->timer = $request->input('timer_count');
        $data->save();

        //insert log quiz
        $question = $request->input('question');
        foreach ($question as $key => $value) {
            $key_answer = $request->input('answer_true_' . $value);
            if (isset($key_answer)) {
                QuizResultLog::create([
                    'id_result' => $data->id,
                    'id_question' => $value,
                    'id_answer' => $key_answer
                ]);
            }
        }

        $data_quiz = QuizResult::find($data->id);
        $data_quiz->score = $this->result_log($request, $data_quiz->id_key, true);
        $data_quiz->save();

        //save total
        $data_score = User_info::find($request->user()->id);
        if ($data_score) {
            $old_score = $data_score->total_score_quiz;
            $data_score->total_score_quiz = $old_score + $data_quiz->score;
            $data_score->save();
        }

        return redirect()->to('quiz/result/' . $id_key);
    }

    public function result_log(Request $request, $id, $get_result = false)
    {
        $data = new \stdClass();
        $data->post = QuizResult::where(['id_key' => $id])->first();
        if (!$data->post && !$get_result) return redirect()->to('quiz');
        if (!$get_result) {
            $data->title = 'ผลการทดแบบทดสอบ ' . $data->post->post->title . ' ของคุณ ' . $data->post->post->user->name;
            $data->des = $data->post->post->content;
        }
        $data->score = 0;
        foreach ($data->post->quiz_result_log as $key => $value) {
            $get_question = PostsQuiz::find($value->id_question);
            if ($get_question && $get_question->id_answer == $value->id_answer) {
                $data->score += 1;
            }
        }
        if (!$get_result) {
            return view('quiz/result_log', ['data' => $data]);
        } else {
            return $data->score;
        }
    }

    public function quiz_history(Request $request)
    {
        if (!Auth::check()) return redirect()->to('login');
        $data = new \stdClass();
        $data->list_history = QuizResult::where(['id_user' => $request->user()->id])
            ->orderBy('created_at', 'desc')
            ->paginate(20);
        return view('quiz/quiz_history', ['data' => $data]);
    }

    public function quiz_rank(Request $request)
    {
        $data = new \stdClass();
        $data->title = 'อันดับสมาชิกที่มีคะแนนสูงสุด 50 คนแรก';
        $data->list_rank = User_info::orderBy('total_score_quiz', 'desc')->limit(50)->get();
        $data->total_player = User::count();
        $data->total_quiz = Posts::where('post_type', 'quiz')->count();
        return view('quiz/quiz_rank', ['data' => $data]);
    }
}
