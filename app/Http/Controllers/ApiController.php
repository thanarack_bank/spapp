<?php

namespace App\Http\Controllers;

use App\Posts;
use Illuminate\Http\Request;

class ApiController extends Controller
{
    public function index(Request $request)
    {
        $id = (int)$request->input('id');

        //if (!$id) return 'Api not found';

        $data = new \stdClass();

        //$data->lists_post = Posts::where(['id_cate'=>'','active' => 'publish'])->paginate(20);

        $data->lists_post = Posts::where(['active' => 'publish', 'post_type' => 'post'])->orderBy('id', 'desc')->paginate(20);

        return view('api.news', ['data' => $data]);
    }
}
