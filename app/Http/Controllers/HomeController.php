<?php

namespace App\Http\Controllers;

use App\Http\FilesApp;
use App\Posts;
use App\User;
use App\User_info;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Validator;

class HomeController extends Controller
{
    public function __construct()
    {
        // $this->middleware('auth');
    }

    public function index(Request $request)
    {
        // Set class
        $data = new \stdClass();

        // Set meta
        $data->title = config_db('web_title');
        $data->des = config_db('web_desc');

        // get new post
        $data->last_post = Posts::where(['active' => 'publish', 'post_type' => 'post'])
            ->orderBy('updated_at', 'desc')
            ->limit(15)
            ->get();

        // Render
        return view('home', ['data' => $data]);
    }

    public function about_page(Request $request)
    {

        $data = new \stdClass();
        $data->about = Posts::where(['slug' => 'about'])->first();
        if (!$data->about) return redirect()->to('/');
        $data->title = $data->about->title;
        return view('about', ['data' => $data]);
    }

    public function api_page(Request $request)
    {
        $data = new \stdClass();
        $data->title = 'ข่าวติดเว็บ';
        return view('api', ['data' => $data]);
    }

    public function profile(Request $request)
    {
        if (!Auth::check()) return redirect()->to('login');
        $data = new \stdClass();
        $data->title = 'ข้อมูลของฉัน';
        $data->user = $request->user();
        return view('profile', ['data' => $data]);
    }

    public function change_password(Request $request)
    {
        if (!Auth::check()) return redirect()->to('login');
        $data = new \stdClass();
        $data->title = 'เปลี่ยนรหัสผ่าน';
        $data->user = $request->user();
        return view('password', ['data' => $data]);
    }

    public function password_save(Request $request)
    {
        $user = $request->user();
        $validator = Validator::make($request->all(), [
            'Old_password' => 'required|min:6',
            'New_password' => 'required|min:6',
            'Confirm_password' => 'required|min:6|same:New_password'
        ], ['Confirm_password.same' => 'รหัสผ่านไม่ตรงกันกรุณาเช็คใหม่อีกครั้ง']);
        if (!Hash::check($request->input('Old_password'), $user->password)) {
            $validator->getMessageBag()->add('password', 'รหัสผ่านเดิมผิด');
            return back()->withErrors($validator);
        }
        if ($validator->fails()) {
            return back()->withErrors($validator);
        } else {
            $obj_user = User::find($request->user()->id);
            $obj_user->password = Hash::make($request->input('Confirm_password'));;
            $obj_user->save();
            return back()->with('success-save', true);
        }
    }

    public function profile_save(Request $request)
    {

        if ($_FILES['file']['size'] > 0) {
            $fileApp = new FilesApp();
            $upload = $fileApp->uploadToServer(Input::file(), false);
            if (isset($uploadIcon->error)) return redirect()->back()->withErrors($upload->error->errors()->all());
        }

        $post = $request->input();
        $user = User::find($request->user()->id);
        $user->name = $post['name'];
        $user->save();

        $user = User_info::find($request->user()->id);
        $user->tel = $post['tel'];
        $user->sex = $post['sex'];
        if (isset($upload['file']->id)) {
            $fileApp->removeFile($user->avatar);
            $user->avatar = $upload['file']->id;
        }
        $user->save();
        return back()->with('success-save', true);
    }
}
