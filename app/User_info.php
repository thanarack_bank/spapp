<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class User_info extends Model
{
    protected $table = 'users_info';

    protected $primaryKey = 'id_user';

    public $timestamps = false;

    protected $fillable = [
        'id_user', 'position', 'total_score_quiz', 'tel', 'sex'
    ];

    public function user()
    {
        return $this->hasOne('App\User', 'id', 'id_user');
    }

    public function files()
    {
        return $this->hasOne('App\Files', 'id', 'avatar');
    }

}
