@extends('layouts.app')

@section('content')

    <section id="content">
        <div class="ui container">
            <div class="ui segment">
                <h3 class="ui dividing header header-h-new">
                    {{$data->title}}
                </h3>
                <div style="min-height: 300px;">
                    <form class="ui form" method="get">
                        <div class="field">
                            <label>ค้นหา ตำแหน่งงานว่าง / แบบทดสอบ / อื่นๆ</label>
                            <input type="text" name="q" placeholder="" value="{{Request::input('q')}}" required
                                   maxlength="">
                        </div>
                    </form>

                    <script>
                        (function () {
                            var cx = '008379547729943320663:tkalrghpyhe';
                            var gcse = document.createElement('script');
                            gcse.type = 'text/javascript';
                            gcse.async = true;
                            gcse.src = 'https://cse.google.com/cse.js?cx=' + cx;
                            var s = document.getElementsByTagName('script')[0];
                            s.parentNode.insertBefore(gcse, s);
                        })();
                    </script>
                    <gcse:searchresults-only></gcse:searchresults-only>
                </div>
            </div>
        </div>
    </section>
@endsection