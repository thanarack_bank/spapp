<div class="ui menu {{(request()->is('/') ? 'home-menu':'page-menu')}} ">
    <div class="ui container">
    <a class="launch icon item">
      <i class="content icon"></i>
    </a>
        <a class="item" href="{{url('/')}}">
            <img src="{{config_db('icon_web')}}" title="{{env('APP_NAME')}}">
        </a>


      	  <div class="ui dropdown item box-top-res">
            <i class="alarm outline icon"></i> ข่าวประกาศ <i class="dropdown icon"></i>

            <div class="menu">
                @foreach(request()->allCategory as $key=>$value)
                    <a class="item" href="{{url('category/'.$value->slug)}}">{{$value->title}}</a>
                @endforeach
            </div>
        </div>
        <a class="item box-top-res" href="{{url('quiz')}}"><i class="wpforms icon"></i> แบบทดสอบออนไลน์</a>
        <a class="item box-top-res" href="{{url('api')}}"><i class="newspaper icon"></i> ข่าวติดเว็บ</a>

        <a class="item box-top-res" href="{{url('about')}}"><i class="coffee icon"></i>เกี่ยวกับเรา</a>
            @if(!Auth::check())
                @if(config_db('enable_member') == 'on')
                    <a class="item box-top-res" href="{{url('register')}}"><i class="signup icon"></i> สมัครสมาชิก</a>
                @endif
                <a class="item box-top-res" href="{{url('login')}}"><i class="sign in icon"></i> เข้าสู่ระบบ</a>
            @else
                <div class="ui dropdown right item box-top-res">
                    <i class="user icon"></i> {{Auth::user()->name}}
                    <i class="dropdown icon"></i>

                    <div class="menu">
                        @if(Auth::user()->user_info->position == 'admin')
                            <a class="item" href="{{url('admin/dashboard')}}"><i class="dashboard icon"></i> หน้าจัดการ</a>
                        @endif
                        <a class="item" href="{{url('profile')}}"><i class="address card icon"></i> จัดการข้อมูลส่วนตัว</a>
                        <a class="item" href="{{url('profile/password')}}"><i class="lock icon"></i>
                            เปลี่ยนรหัสผ่าน</a>
                        <a class="item" href="{{url('quiz/history')}}"><i class="history icon"></i> ประวัติทำข้อสอบ</a>
                        <a class="item" href="javascript:void(0)"
                           onclick="event.preventDefault();document.getElementById('logout').submit();"><i
                                    class="sign out icon"></i> ออกจากระบบ</a>
                    </div>
                </div>
            @endif
            <form style="display: none" id="logout" class="box-top-res" method="post"
                  action="{{route('logout')}}">{{csrf_field()}}</form>

    </div>
</div>

<div class="pusher">
<div class="ui left demo vertical inverted sidebar labeled icon menu">
    <a class="item"  href="{{url('quiz')}}">
    แบบทดสอบออนไลน์
  </a>
  <a class="item" href="{{url('api')}}">
    ข่าวติดเว็บ
  </a>
      <a class="item" href="{{url('about')}}">
    เกี่ยวกับเรา
  </a>
    @foreach(request()->allCategory as $key=>$value)
                    <a class="item" href="{{url('category/'.$value->slug)}}">{{$value->title}}</a>
                @endforeach
   @if(!Auth::check())
   @if(config_db('enable_member') == 'on')
  <a class="item" href="{{url('login')}}">
    เข้าสู่ระบบ
  </a>
   @endif
  <a class="item" href="{{url('register')}}">
    สมัครสมาชิก
  </a>
  @else
  @if(Auth::user()->user_info->position == 'admin')
                            <a class="item" href="{{url('admin/dashboard')}}"> หน้าจัดการ</a>
                        @endif
  <a class="item" href="{{url('profile')}}"> จัดการข้อมูลส่วนตัว</a>
                        <a class="item" href="{{url('profile/password')}}">
                            เปลี่ยนรหัสผ่าน</a>
                        <a class="item" href="{{url('quiz/history')}}"> ประวัติทำข้อสอบ</a>
                        <a class="item" href="javascript:void(0)"
                           onclick="event.preventDefault();document.getElementById('logout').submit();">ออกจากระบบ</a>
  @endif
</div>

</div>