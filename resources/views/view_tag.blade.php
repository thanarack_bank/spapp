@extends('layouts.app')

@section('content')
    <section>
        <div class="ui grid stackable container">
            <div class="three wide column">
                @include('slide')
            </div>
            <div class="thirteen wide column">
                <div class="content-page-view">
                    <h3 class="ui dividing header header-h-new">
                        {{$data->type_txt.' : '.$data->title}}
                    </h3>

                    @if(isset($data->last_post))
                        <div class="ui three stackable cards">
                            @foreach($data->last_post as $key=>$value)
                                <div class="ui card">
                                    <a class="image" href="{{url('view/'.$value->id)}}">
                                        <div class="card-thumb"
                                             style="background: url('{{fileConvertUrl($value->files,true)}}')"></div>
                                    </a>

                                    <div class="content">
                                        <a class="header title-post"
                                           href="{{url('view/'.$value->id)}}">{{$value->title}}</a>

                                        <div class="meta">
                                            <p class="by">ที่มา
                                                : {{isset($value->org->title) ? $value->org->title : null}}</p>

                                            <p class="date">{{$value->updated_at->format('j M Y')}}</p>
                                        </div>
                                    </div>
                                </div>
                            @endforeach
                        </div>
                    @endif
                    @if($data->last_post->count()>0)
                        <div class="page-zone">
                            {{ $data->last_post->links('vendor/pagination/default') }}
                        </div>
                    @endif
                </div>
            </div>
        </div>
    </section>
@endsection

