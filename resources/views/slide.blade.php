<div class="ui vertical menu">
    <div class="item">
        <form class="ui transparent icon input" action="{{url('search')}}" method="get">
            <input type="text" placeholder="ค้นหางาน" name="q">
            <i class="search icon"></i>
        </form>
    </div>
    @foreach(request()->allCategory as $key=>$value)
        <a class="teal item" href="{{url('category/'.$value->slug)}}">
            {{$value->title}}
        </a>
    @endforeach
</div>