@extends('layouts.app')

@section('content')
    <section id="view_post">
        <div class="ui grid doubling stackable container">
            <div class="three wide column">
                @include('slide-member')
            </div>
            <div class="thirteen wide column">
                <div class="content-page-view">
                    @include('alert-html')
                    <div class="ui segment" style="margin-top: 0;">
                        <h3 class="ui dividing header header-h-new">
                            เปลี่ยนรหัสผ่าน
                        </h3>
                        <div class="content">
                            <form class="ui form" method="post">
                                {{csrf_field()}}
                                <div class="field">
                                    <label>รหัสเดิม</label>
                                    <input  type="password" name="Old_password" require placeholder="" >
                                </div>
                                <div class="field">
                                    <label>รหัสผ่านใหม่</label>
                                    <input type="password" name="New_password" required placeholder="">
                                </div>
                                <div class="field">
                                    <label>รหัสผ่านใหม่</label>
                                    <input type="password" name="Confirm_password" required placeholder="">
                                </div>
                                <br/>
                                <div align="right">
                                    <button class="ui primary button" type="submit">อัพเดท</button>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection
