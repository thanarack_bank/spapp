@extends('layouts.app')

@section('content')
    <section>
        <div class="ui container">
            <div class="ui segment">
                <h3 class="ui dividing header header-h-new">
                    เข้าสู่ระบบ
                </h3>

                <div class="ui centered  grid">
                    <div class="column">
                        <form class="ui form" role="form" method="POST" action="{{ route('login') }}">
                            {{ csrf_field() }}
                            @if ($errors->has('email'))
                                <div class="ui negative message">
                                    <i class="close icon"></i>
                                    <div class="header">
                                        {{trans('auth.login_fails')}}
                                    </div>
                                    <p>{{ $errors->first('email') }}
                                    </p></div>
                            @endif
                            @if ($errors->has('password'))
                                <div class="ui negative message">
                                    <i class="close icon"></i>
                                    <div class="header">
                                        {{trans('auth.login_fails')}}
                                    </div>
                                    <p>{{ $errors->first('password') }}
                                    </p></div>
                            @endif
                            @if(session()->get('user-ban'))
                                <div class="ui negative message">
                                    <i class="close icon"></i>
                                    <div class="header">
                                        {{trans('auth.login_fails')}}
                                    </div>
                                    <p>User นี้ไม่สามารถเข้าใช้งานได้ เนื่องจากโดนแบน</p></div>
                            @endif
                            <div class="field">
                                <label>อีเมล์</label>
                                <input id="email" type="email" placeholder="อีเมล์" value="{{ old('email') }}" required
                                       autofocus name="email">
                            </div>
                            <div class="field">
                                <label>รหัสผ่าน</label>
                                <input id="password" type="password" name="password" required>
                            </div>
                            <div class="field">
                                <div class="ui checkbox">
                                    <input type="checkbox" name="remember"
                                           {{ old('remember') ? 'checked' : '' }} tabindex="0"
                                           class="hidden">
                                    <label>จดจำไว้ในระบบ</label>
                                </div>
                            </div>
                            <button class="ui button primary" type="submit">ตกลง</button>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection

@section('sc')
    <script>
        $('.ui.checkbox').checkbox();
    </script>
@endsection
