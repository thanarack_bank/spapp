@extends('layouts.app')

@section('content')
    <section>
        <div class="ui container">
            <div class="ui segment">
                <h3 class="ui dividing header header-h-new">
                    ลงทะเบียน
                </h3>

                <div class="ui centered  grid">
                    <div class="column" style="min-height: 300px;">
                        @if(config_db('enable_member') != 'on')
                            <div class="ui negative message">
                                <i class="close icon"></i>

                                <div class="header">
                                    {{trans('auth.register_fails')}}
                                </div>
                                <p>ระบบสมัครสมาชิกปิดชั่วคราว
                                </p></div>
                            <div align="center">
                                <a class="ui primary button" href="{{url('/')}}">กลับหน้าหลัก</a>
                            </div>
                        @else
                            <form class="ui form" role="form" method="POST" action="{{ route('register') }}">
                                {{ csrf_field() }}
                                @if ($errors->has('name'))
                                    <div class="ui negative message">
                                        <i class="close icon"></i>

                                        <div class="header">
                                            {{trans('auth.register_fails')}}
                                        </div>
                                        <p>{{ $errors->first('name') }}
                                        </p></div>
                                @endif
                                @if ($errors->has('email'))
                                    <div class="ui negative message">
                                        <i class="close icon"></i>

                                        <div class="header">
                                            {{trans('auth.register_fails')}}
                                        </div>
                                        <p>{{ $errors->first('email') }}
                                        </p></div>
                                @endif
                                @if ($errors->has('password'))
                                    <div class="ui negative message">
                                        <i class="close icon"></i>

                                        <div class="header">
                                            {{trans('auth.register_fails')}}
                                        </div>
                                        <p>{{ $errors->first('password') }}
                                        </p></div>
                                @endif
                                <div class="field">
                                    <label>ชื่อ - สกุล</label>
                                    <input id="name" type="text" name="name" value="{{ old('name') }}"
                                           required autofocus>
                                </div>
                                <div class="field">
                                    <label>อีเมล์</label>
                                    <input id="email" type="email" name="email" value="{{ old('email') }}"
                                           required>
                                </div>
                                <div class="field">
                                    <label>รหัสผ่าน</label>
                                    <input id="password" type="password" name="password" required>
                                </div>
                                <div class="field">
                                    <label>รหัสผ่านอีกครั้ง</label>
                                    <input id="password-confirm" type="password"
                                           name="password_confirmation" required>
                                </div>
                                <button class="ui button primary" type="submit">สมัคร</button>
                            </form>
                        @endif
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection
