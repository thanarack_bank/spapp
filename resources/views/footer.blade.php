<section id="footer">
    <div class="footer">
        <div class="ui container">
            <div class="ui two column stackable grid">
                <div class="ten wide column">
                    <h5 class="f-about">เกี่ยวกับเรา</h5>

                    <div class="ui row">
                        {{config_db('about_footer')}}
                    </div>
                </div>
                <div class="six wide column">
                    <h5 class="f-about">ช่องทางติดต่อ</h5>

                    <div class="ui four column grid f-icon">
                        <div class="column"><a href="{{config_db('link_facebook')}}"><i class="facebook icon"></i></a>
                        </div>
                        <div class="column"><a href="{{config_db('link_twitter')}}"><i class="twitter Square icon"></i></a>
                        </div>
                        <div class="column"><a href="{{config_db('link_youtube')}}"><i class="youtube Square icon"></i></a>
                        </div>
                        <div class="column"><a href="{{config_db('link_google')}}"><i
                                        class="google Plus Square icon"></i></a></div>
                    </div>
                    <div class="ui row f-contact">
                        <p><strong>Email</strong> : {{config_db('email')}}</p>
                        <p><strong>Tel</strong> : {{config_db('tel')}} </p>
                        <p><strong>Line</strong> : {{config_db('line')}}</p>
                    </div>

                </div>
            </div>
            <div class="ui grid">
                <div class="column">
                    <div class="final-footer">
                        © {{date('Y')}} All rights reserved Design by : <a href="http://thungna.com" target="_blank">Thungna.com</a>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>