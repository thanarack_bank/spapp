<div class="ui vertical menu">

    <div class="item">
        <div class="header">เมนูผู้ใช้</div>
        <div class="menu">
            <a class="item" href="{{url('profile')}}"><i class="arrow left icon"></i> ข้อมูลส่วนตัว</a>
            <a class="item" href="{{url('profile/password')}}"><i class="arrow left icon"></i> เปลี่ยนรหัสผ่าน</a>
            <a class="item" href="{{url('rank')}}"><i class="arrow left icon"></i> อันดับคะแนน</a>
            <a class="item" href="javascript:void(0)"
               onclick="event.preventDefault();document.getElementById('logout').submit();"><i class="arrow left icon"></i> ออกจากระบบ</a>
        </div>
    </div>
</div>