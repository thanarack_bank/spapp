@extends('layouts.app')

@section('content')
    <section id="view_post">
        <div class="ui grid  stackable container">
            <div class="three wide column">
                @include('slide')
            </div>
            <div class="thirteen wide column">
                <div class="content-page-view">
                    <div class="ui card-page-view card">
                        <div class="content">
                            <div class="header title-card"><h2 title="{{$data->title}}">{{$data->title}}</h2></div>
                            <div class="meta"> อัพเดตเมื่อ : {{$data->updated_at->format('j M Y H:i')}}</div>
                            <div class="meta"> หน่วยงาน
                                : {{isset($data->org->title) ? $data->org->title : null}}</div>
                        </div>
                        <div class="extra content description">
                            {!! config_db('ads_post_01') !!}
                            {!! htmlspecialchars_decode($data->content) !!}
                            {!! config_db('ads_post_02') !!}
                        </div>
                        <div class="extra content  shared-log" align="center">
                            <p class="txt-s"><i class="share icon"></i> แชร์ข่าวนี้</p>
                            <button class="ui facebook button"
                                    onclick="PopupCenter('{{Request::url()}}','{{$data->title}}','780','450','facebook')">
                                <i class="facebook icon"></i>
                                Facebook
                            </button>
                            <button class="ui twitter button"
                                    onclick="PopupCenter('{{Request::url()}}','{{$data->title}}','780','450','twitter')">
                                <i class="twitter icon"></i>
                                Twitter
                            </button>
                            <button class="ui google plus button"
                                    onclick="PopupCenter('{{Request::url()}}','{{$data->title}}','780','450','google')">
                                <i class="google plus icon"></i>
                                Google Plus
                            </button>
                        </div>
                        <div class="extra content">
                            <p><i class="eye icon"></i>
                                <span class="view_count">{{$data->views}} ครั้ง</span></p>

                            <p class="post-tags"><strong>Tag:</strong>
                                @foreach($tags as $key=>$value)
                                    <a class="ui blue small tag label"
                                       href="{{url('tag/'.$value->slug)}}">{{$value->title}}</a>
                                @endforeach
                            </p>
                        </div>
                    </div>
                    @if($data->relate_post->count() != 0)
                        <h4 class="ui dividing header header-h-new">
                            ข่าวอื่นๆ
                        </h4>
                        <div class="ui three stackable cards">
                            @foreach($data->relate_post as $key=>$value)
                                <div class="ui card">
                                    <a class="image" href="{{url('view/'.$value->id)}}">
                                        <div class="card-thumb"
                                             style="background: url('{{fileConvertUrl($value->files,true)}}')"></div>
                                    </a>
                                    <div class="content">
                                        <a class="header title-post"
                                           href="{{url('view/'.$value->id)}}">{{$value->title}}</a>

                                        <div class="meta">
                                            <p class="by">ที่มา
                                                : {{isset($value->org->title) ? $value->org->title : '-'}}</p>

                                            <p class="date">{{$value->updated_at->format('j M Y')}}</p>
                                        </div>
                                    </div>
                                </div>
                            @endforeach
                        </div>
                    @endif
                </div>
            </div>
        </div>
    </section>
@endsection


@section('sc')
    <script>
        $(function () {
            $('.extra.content.description img').addClass('ui rounded bordered image');
        });
    </script>
@endsection