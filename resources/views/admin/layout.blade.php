@extends('layouts.app')

@section('content')
    <section id="view_post">
        <div class="ui grid doubling stackable container">
            <div class="three wide column">
                @include('admin.slide')
            </div>
            <div class="thirteen wide column">
                <div class="content-page-view">
                    @include('alert-html')
                    @yield('content-admin')
                </div>
            </div>
        </div>
    </section>
@endsection

@section('sc')
    @yield('sc-admin')
@endsection