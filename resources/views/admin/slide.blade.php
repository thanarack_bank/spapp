<div class="ui vertical menu">
    <div class="item">
        <a class="header" href="{{url('admin/dashboard')}}">แดชบอร์ด</a>
    </div>
    <div class="item">
        <div class="header">โพส</div>
        <div class="menu">
            <a class="item" href="{{url('admin/post/new')}}">โพสใหม่</a>
            <a class="item" href="{{url('admin/post/list')}}">จัดการโพส</a>
            <a class="item" href="{{url('admin/category/new')}}">เพิ่มหมวดหมู่</a>
            <a class="item" href="{{url('admin/category/org')}}">จัดการหน่วยงาน</a>
            <a class="item" href="{{url('admin/category/menu')}}">จัดการเมนู</a>
            <a class="item" href="{{url('admin/category/tag')}}">จัดการแท็ค</a>
            <a class="item" href="{{url('admin/category/quiz')}}">จัดการหมวดหมู่ข้อสอบ</a>
        </div>
    </div>
    <div class="item">
        <div class="header">แบบทดสอบ</div>
        <div class="menu">
            <a class="item" href="{{url('quiz/new')}}">เพิ่มแบบทดสอบ</a>
            <a class="item" href="{{url('admin/quiz/list')}}">จัดการแบบทดสอบ</a>
        </div>
    </div>
    <div class="item">
        <div class="header">สมาชิก</div>
        <div class="menu">
            <a class="item" href="{{url('admin/member')}}">จัดการสมาชิก</a>
        </div>
    </div>
    <div class="item">
        <div class="header">ไฟล์</div>
        <div class="menu">
            <a class="item" href="{{url('admin/files')}}">จัดการไฟล์</a>
        </div>
    </div>
    <div class="item">
        <div class="header">ตั้งค่า</div>
        <div class="menu">
            <a class="item" href="{{url('admin/page/about')}}">เกี่ยวกับเรา</a>
            <a class="item" href="{{url('admin/settings')}}">ทั่วไป</a>
            <a class="item" href="{{url('admin/settings/social')}}">โซเชียล</a>
            <a class="item" href="{{url('admin/settings/ads')}}">โฆษณา</a>
        </div>
    </div>
</div>