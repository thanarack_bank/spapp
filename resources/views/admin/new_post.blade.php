@extends('admin.layout')

@section('header')

@endsection

@section('content-admin')
    <div id="new_post">
        <div class="ui segment">
            <h3 class="ui dividing header header-h-new">
                โพสใหม่
            </h3>

            <div class="ui doubling grid">
                <div class="column">
                    <form class="ui form" method="post" enctype="multipart/form-data">
                        {{csrf_field()}}
                        @if(isset($data->post))
                            <input type="hidden" name="id_update" value="{{$data->post->id}}">
                        @endif
                        <div class="field" align="center">
                            <div class="inline fields">
                                <label for="fruit">สถานะโพส :</label>
                                <div class="field">
                                    <div class="ui radio checkbox">
                                        <input type="radio" name="active"
                                               value="daft"
                                               {{isset($data->post) && $data->post->active == 'daft' ? 'checked':(empty($data->post ) ? 'checked' :null)}}
                                               tabindex="0"
                                               class="hidden">
                                        <label>แบบร่าง</label>
                                    </div>
                                </div>
                                <div class="field">
                                    <div class="ui radio checkbox">
                                        <input type="radio" name="active"
                                               {{isset($data->post) && $data->post->active == 'publish' ? 'checked':''}}
                                               value="publish"
                                               tabindex="0" class="hidden">
                                        <label>สาธารณะ</label>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="field">
                            <label>หัวข้อ / ชื่อเรื่อง</label>
                            <input type="text" name="title" placeholder=""
                                   value="{{isset($data->post) ? $data->post->title:null}}" required maxlength="200">
                        </div>
                        <div class="field">
                            <label>หน่วยงาน</label>
                            <select class="ui search dropdown" name="org">
                                @foreach($data->list_org as $key=>$value)
                                    <option {{isset($data->post) && $value->id== $data->post->org_id ? 'selected':null}} value="{{$value->id}}">{{$value->title}}</option>
                                @endforeach
                            </select>
                        </div>
                        <div class="field">
                            <label>หมวดหมู่โพส</label>
                            <select multiple="" class="ui dropdown" name="category[]">
                                @foreach($data->list_category as $key=>$value)
                                    <option value="{{$value->slug}}" {{isset($data->post) &&  in_array($value->id,$data->list_tags) ? 'selected':null}}>{{$value->title}}</option>
                                @endforeach
                            </select>
                        </div>
                        <div class="field">
                            <label>รายละเอียด</label>
                            <p>
                                <button class="ui tiny primary basic button" type="button"
                                        onclick="filesSelect('image','1','textarea','content-post')">เพิ่มรูป</button>
                                <button class="ui tiny primary basic button" type="button"
                                        onclick="filesSelect('files','1','textarea','content-post')">
                                    เพิ่มไฟล์แนบ
                                </button>
                                <button class="ui tiny primary basic button" onclick="uploadTheFile(false)" type="button">
                                    อัพโหลดไฟล์
                                </button>
                            </p>
                            <textarea name="content"
                                      id="content-post">{{isset($data->post) ? $data->post->content:null}}</textarea>
                        </div>
                        <div class="field">
                            <label>รูปหน้าปก</label>
                            <button class="ui tiny primary basic button" type="button"
                                    onclick="filesSelect('image','1',null,'#list_file_post')">
                                รูปหน้าปก
                            </button>
                            <div id="list_file_post" style="margin-top: 10px;">
                                @if(isset($data->post) && $data->post->files)
                                    <div class="ui card" id="file_id_thumb_{{$data->post->id_file}}">
                                        <input type="hidden" name="id_files[]" value="{{$data->post->id_file}}">
                                        <input type="hidden" name="unique_id[]" value="{{time()}}">
                                        <div class="image">
                                            <div class="card-thumb" style="background: url('{{fileConvertUrl($data->post->files,true)}}')"></div>
                                        </div>
                                        <div class="content">
                                            <p align="center">
                                                <button type="button" onclick="removeThumb({{$data->post->id_file}})"
                                                        class="ui small icon red button"><i class="trash icon"></i>
                                                </button>
                                            </p>
                                        </div>
                                    </div>
                                @endif
                            </div>
                        </div>
                        <div class="field">
                            <label>แท็ค <span class="introduce-label">ตัวอย่างแท็คให้คั่นคำด้วย คอมม่า ( , ) เช่น นักวิชาการ,พนักงานบัญชี</span></label>
                            <div class="ui right labeled left icon input">
                                <i class="tags icon"></i>
                                <input type="text" placeholder="เพิ่มแท็คลงโพส" name="tag"
                                       value="{{isset($data->post) && !empty($data->list_tags2) ? implode(',',$data->list_tags2):null}}">
                                <a class="ui tag label">
                                    คั่นด้วย , (คอมม่า)
                                </a>
                            </div>
                        </div>
                        <br/>
                        <div align="right">
                            <button class="ui button large primary" type="submit">บันทึก</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('sc')
    <script src="{{asset('tinymce/js/tinymce/tinymce.min.js')}}"></script>
    <script src="{{asset('tinymce/setup.js?v=1')}}"></script>
    <script src="{{asset('plugin/dropzone/dropzone.js')}}"></script>
@endsection