@extends('admin.layout')

@section('content-admin')
    <div id="addQuestion"></div>
    <div id="new_post">
        <div class="ui segment">
            <h3 class="ui dividing header header-h-new">
                @if(isset($data->post))
                    แก้ไข : {{$data->post->title}}
                @else
                    เพิ่มแบบทดสอบ
                @endif
            </h3>
            <div class="ui doubling grid">
                <div class="column">
                    <form class="ui form" method="post" enctype="multipart/form-data" id="quiz_form">
                        {{csrf_field()}}
                        @if(isset($data->post))
                            <input type="hidden" name="id_update" value="{{$data->post->id}}">
                        @endif
                        <div class="field" align="center">
                            <div class="inline fields">
                                <label for="fruit">สถานะแบบทดสอบ :</label>
                                <div class="field">
                                    <div class="ui radio checkbox">
                                        <input type="radio" name="active"
                                               value="daft"
                                               {{isset($data->post) && $data->post->active == 'daft' ? 'checked':(empty($data->post ) ? 'checked' :null)}}
                                               tabindex="0"
                                               class="hidden">
                                        <label>แบบร่าง</label>
                                    </div>
                                </div>
                                <div class="field">
                                    <div class="ui radio checkbox">
                                        <input type="radio" name="active"
                                               {{isset($data->post) && $data->post->active == 'publish' ? 'checked':''}}
                                               value="publish"
                                               tabindex="0" class="hidden">
                                        <label>สาธารณะ</label>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="field">
                            <label>หัวข้อ / ชื่อเรื่อง / แบบทดสอบ</label>
                            <input type="text" name="title" placeholder=""
                                   value="{{isset($data->post) ? $data->post->title:null}}" required maxlength="200">
                        </div>
                        <div class="field">
                            <label>คำอธิบายข้อสอบ</label>
                            <textarea name="content">{{isset($data->post) ? $data->post->content:null}}</textarea>
                        </div>
                        <div class="field">
                            <label>หมวดหมู่ข้อสอบ</label>
                            <select multiple="" class="ui dropdown" name="category[]">
                                @foreach($data->list_category as $key=>$value)
                                    <option value="{{$value->slug}}" {{isset($data->post) &&  in_array($value->id,$data->list_tags) ? 'selected':null}}>{{$value->title}}</option>
                                @endforeach
                            </select>
                        </div>
                        <div class="field">
                            <label>รูปหน้าปก</label>
                            <button class="ui tiny primary basic button" type="button"
                                    onclick="filesSelect('image','1',null,'#list_file_post')">
                                รูปหน้าปก
                            </button>
                            <button class="ui tiny primary basic button" onclick="uploadTheFile(false)" type="button">
                                อัพโหลดไฟล์
                            </button>
                            <div id="list_file_post" style="margin-top: 10px;">
                                @if(isset($data->post) && $data->post->files)
                                    <div class="ui card" id="file_id_thumb_{{$data->post->id_file}}">
                                        <input type="hidden" name="id_files[]" value="{{$data->post->id_file}}">
                                        <input type="hidden" name="unique_id[]" value="{{time()}}">
                                        <div class="image">
                                            <div class="card-thumb" style="background: url('{{fileConvertUrl($data->post->files,true)}}')"></div>
                                        </div>
                                        <div class="content">
                                            <p align="center">
                                                <button type="button" onclick="removeThumb({{$data->post->id_file}})"
                                                        class="ui small icon red button"><i class="trash icon"></i>
                                                </button>
                                            </p>
                                        </div>
                                    </div>
                                @endif
                            </div>
                        </div>
                        <div class="field">
                            <label>รายการข้อสอบ</label>
                            <button class="ui tiny primary basic button" type="button" onclick="addQuestion()">
                                Add Question
                            </button>
                            <button class="ui tiny red basic button" type="button" onclick="clearQuestion()">
                                Clear Question
                            </button>
                            <div id="list_question_post" style="margin-top: 10px;">
                                @if(isset($data->post))
                                    @foreach($data->list_question as $key=> $value)
                                        <div class="question_box" id="id_qt_{{$key+1}}">
                                            <input name="id_quiz_edit[]" value="{{$value->id}}" type="hidden">
                                            <table style="margin-bottom: 2em;" class="ui single line table">
                                                <thead>
                                                <tr>
                                                    <th width="25%" style="text-align: center"><span
                                                                style="color: red;cursor: pointer;"
                                                                class="question-click-remove"
                                                                onclick="removeQuestion({{$key+1}})"><i
                                                                    class="trash icon"></i></span> คำถามข้อที่ <span
                                                                class="number-of-question">{{$key+1}}</span></th>
                                                    <th>
                                                        <div class="ui small input"><input type="text" maxlength="300"
                                                                                           required=""
                                                                                           value="{{$value->title}}"
                                                                                           name="question[]">
                                                        </div>
                                                    </th>
                                                </tr>
                                                </thead>
                                                <tbody>
                                                @foreach($value->answer_quiz as $key_2 => $value_2)
                                                    <input name="id_quiz_answer_{{$value->id}}[]"
                                                           value="{{$value_2->id}}" type="hidden">
                                                    <tr>
                                                        <td style="text-align: center">
                                                            <div class="field">
                                                                <div class="ui radio checkbox">
                                                                    <input type="radio"
                                                                           class="answer-true"
                                                                           {{($value->id_answer == $value_2->id ? 'checked':null)}}
                                                                           value="{{$value_2->id}}"
                                                                           name="answer_true_{{$key+1}}"
                                                                           tabindex="0">
                                                                    <label
                                                                            style="color: #c3c3c3;">คำตอบที่
                                                                        <span>{{$key_2+1}}</span></label>
                                                                </div>
                                                            </div>
                                                        </td>
                                                        <td>
                                                            <div class="ui small input"><input type="text"
                                                                                               class="title-answer"
                                                                                               value="{{$value_2->title}}"
                                                                                               name="answer_{{$key+1}}[]">
                                                            </div>
                                                        </td>
                                                    </tr>
                                                @endforeach
                                                </tbody>
                                            </table>
                                        </div>
                                    @endforeach
                                @endif
                            </div>
                        </div>
                        <br/>
                        <div align="right">
                            <button class="ui button large primary" onclick="submit_quiz(this)" type="button">บันทึก</button>
                            <input style="display:none;" type="submit" id="quiz_submit_btn"/>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('sc')
    <script>
        var count_question = '{{isset($data->post) ? $data->list_question->count()+1 : 1}}';
        count_question = parseInt(count_question);

        function submit_quiz(event){
            $('#quiz_submit_btn').click();
            $(event).prop('disabled', true);
            $(event).text('กำลังทำการตรวจสอบ....');
            setTimeout(function() {
                $(event).text('บันทึก');
                $(event).prop('disabled', false);
            }, 6000);
        }

        function closeModalQuestion() {
            var closeModal = $('#modal-promt-question').modal('hide');
            if (closeModal) {
                $('#modal-promt-question').remove();
            }
        }

        function addQuestion() {
            var modal_question = '<div class="ui small modal" id="modal-promt-question">' +
                '<div class="header">' +
                'เพิ่มคำถาม' +
                '</div>' +
                '<div class="content">' +
                '<form class="ui form">' +
                '<div class="field">' +
                '<div class="two fields">' +
                '<div class="field">' +
                '<label>จำนวนข้อสอบ</label>' +
                '<select class="ui fluid dropdown" id="num_question" >' +
                '<option value="5">5</option>' +
                '<option value="10">10</option>' +
                '<option value="15">15</option>' +
                '<option value="20">20</option>' +
                '<option value="25">25</option>' +
                '<option value="30">30</option>' +
                '</select>' +
                '</div>' +
                '<div class="field">' +
                '<label>จำนวนคำตอบ</label>' +
                '<select class="ui fluid dropdown" id="num_answer" >' +
                '<option value="3">3</option>' +
                '<option value="4">4</option>' +
                '<option value="5">5</option>' +
                '</select>' +
                '</div>' +
                '</div>' +
                '</div>' +
                '</form>' +
                '</div>' +
                '<div class="actions">' +
                ' <div class="ui primary button" onclick="addQuestionPost()">' +
                'เพิ่ม' +
                '</div>' +
                ' <div class="ui button" onclick="closeModalQuestion()">' +
                'ยกเลิก' +
                '</div>' +
                '</div>' +
                '</div>';
            var addQuestion = $('#addQuestion').html(modal_question);
            if (addQuestion) {
                $('#modal-promt-question').modal({
                    closable: false
                }).modal('show');
            }
        }

        function clearQuestion() {
            if (!confirm('ต้องการลบข้อสอบทั้งหมด ?')) return false;
            count_question = 1;
            $('#list_question_post').empty();
        }

        function addQuestionPost() {
            var num_question = $('#num_question').val();
            var num_answer = $('#num_answer').val();
            if (!num_question || !num_answer) return false;
            var exe = $('#list_question_post').append(theme_question_make(num_question, num_answer));
            if (exe) {
                closeModalQuestion();
            }
        }

        function theme_question_make(num_question, num_answer) {
            var theme_sup = '';
            var run_question = count_question;

            if (num_question.length > 0 && num_answer.length > 0) {
                for (var x = 1; x <= num_question; x++) {
                    theme_sup += theme_question(run_question, x, num_answer);
                    run_question += 1;
                }
                count_question = run_question;
            }

            return theme_sup;
        }

        function removeQuestion(id) {
            if (confirm('ต้องการลบข้อนี้ใช่หรือไม่')) {
                var qtData = $('#id_qt_' + id);
                qtData.fadeOut(300, function () {
                    var checkRemove = $(this).remove();
                    if (checkRemove) {
                        $.each($('div.question_box'), function (key, value) {
                            key += 1;
                            $(value).attr('id', 'id_qt_' + key);
                            $(value).find('input.title-question').attr('name', 'qt_' + key);
                            $(value).find('input.title-answer').attr('name', 'answer_' + key + '[]');
                            $(value).find('input.answer-true').attr('name', 'answer_true_' + key);
                            $(value).find('span.number-of-question').text(key);
                            $(value).find('span.question-click-remove').attr('onclick', 'removeQuestion(' + key + ')');
                        });
                        count_question -= 1;
                    }
                });
            }
        }

        function theme_question(count_question, x, num_answer) {
            var answer_sup = '';

            if (num_answer.length >= 1) {
                for (var y = 1; y <= num_answer; y++) {
                    answer_sup += theme_answer(count_question, y);
                }
            }

            var html_question = '<div class="question_box"  id="id_qt_' + count_question + '"><table style="margin-bottom: 2em;" class="ui single line table">' +
                '<thead>' +
                '<tr>' +
                '<th width="25%" style="text-align: center">' +
                '<span style="color: red;cursor: pointer;" class="question-click-remove" onclick="removeQuestion(' + count_question + ')">' +
                '<i class="trash icon"></i></span> คำถามข้อที่ <span class="number-of-question">' + count_question + '</span>' +
                '</th>' +
                '<th>' +
                '<div class="ui small input">' +
                // '<input type="text" class="title-question" required name="qt_' + count_question + '">' +
                '<input type="text" maxlength="300" required name="question[]">' +
                '</div>' +
                '</th>' +
                '</tr>' +
                '</thead>' +
                '<tbody>' +
                answer_sup +
                '</tbody>' +
                ' </table></div>';
            return html_question;
        }

        function theme_answer(count_question, y) {

            var checkIt = '';
            if (y == 1) checkIt = 'checked';

            var html_answer = '<tr>' +
                '<td style="text-align: center">' +
                '<div class="field">' +
                ' <div class="ui radio checkbox">' +
                '<input type="radio" class="answer-true" ' + checkIt + ' value="' + y + '" name="answer_true_' + count_question + '"  tabindex="0" class="hidden">' +
                '<label style="color: #c3c3c3;">คำตอบที่ <span>' + y + '</span></label>' +
                '</div>' +
                ' </div>' +
                '</td>' +
                '<td>' +
                ' <div class="ui small input">' +
                '<input type="text" class="title-answer" name="answer_' + count_question + '[]">' +
                ' </div>' +
                ' </td>' +
                '</tr>';
            return html_answer;
        }

    </script>
    <script src="{{asset('plugin/dropzone/dropzone.js')}}"></script>
@endsection
