<div class="ui grid">
    <div class="row">
        <div class="column">
            <div class="ui three cards">
                @foreach($data->list_files as $value)
                    <div class="card">
                        <div class="image">
                            <div class="card-thumb" style="background: url('{{fileConvertUrl($value,true)}}')"></div>
                        </div>
                        <div class="content" align="center">
                            <p style="text-align: left;height: 20px;overflow: hidden">{{$value->old_name}}</p>
                            <button class="ui small red basic button"
                                    onclick="addToPost('{{$value->id}}','{{fileConvertUrlLink($value,true)}}','{{fileConvertUrlLink($value)}}')">
                                เพิ่ม
                            </button>
                        </div>
                    </div>
                @endforeach
            </div>
            <br/>
            <div align="center">
                {{$data->list_files->links('vendor.pagination.file-json')}}
            </div>
        </div>
    </div>
</div>
