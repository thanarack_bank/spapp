@extends('admin.layout')

@section('content-admin')
    <div id="new_post">
        <div class="ui segment">
            <h3 class="ui dividing header header-h-new">
                จัดการ {{$data->title}}
            </h3>

            <div class="ui doubling grid">
                <div class="column">
                    <div>
                        <table class="ui striped table">
                            <thead>
                            <tr>
                                <th width="45%">Title</th>
                                <th width="19%">Updated</th>
                                <th width="19%">Active</th>
                                <th width="" align="center">Action</th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach($data->list_category as $value)
                                <tr id="id_cate_{{$value->id}}">
                                    <td>{{$value->title}}</td>
                                    <td>{{$value->updated_at}}</td>
                                    <td>
                                        <span class="ui {{$value->active == 'no' ? 'red' : 'green'}} circular label">{{ucfirst($value->active)}}</span>
                                    </td>
                                    <td>
                                        <button class="ui mini icon blue button"
                                                onclick="window.location.href = APP_URL+'admin/category/new?id={{$value->id}}'">
                                            <i class="edit icon"></i>
                                        </button>
                                        <button class="ui mini icon red  button" onclick="removeCate({{$value->id}})">
                                            <i class="trash icon"></i>
                                        </button>
                                    </td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                        <p>พบ {{number_format($data->list_category->total())}} แถว</p>
                        @if($data->list_category->count() > 0)
                            <div align="center">
                                {{$data->list_category->links()}}
                            </div>
                        @endif
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('sc-admin')
    <script>
        function removeCate(id) {
            if (!confirm('ต้องการลบเมนูนี้ใช่หรือไม่ ?')) return false;
            $.post(APP_URL + 'api/remove_cate/' + id, {
                _token: APP_TOKEN
            }, function (res) {
                if (res.status === 100) {
                    $('#id_cate_' + id).remove();
                }
            });
        }
    </script>
@endsection