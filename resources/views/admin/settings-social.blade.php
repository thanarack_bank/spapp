@extends('admin.layout')

@section('content-admin')
    <div id="settings">

        <form class="ui form" method="post" action="{{url('admin/settings')}}" enctype="multipart/form-data">
            <div class="ui segment">
                <h3 class="ui dividing header header-h-new">
                    ตั้งค่า - โซเซียล
                </h3>

                <div class="ui doubling grid">
                    <div class=" column">
                        {{csrf_field()}}
                        <div class="field">
                            <label>Link facebook</label>
                            <input type="text" placeholder="" name="link_facebook"
                                   value="{{!empty($setting['link_facebook']) ? $setting['link_facebook'] : null}}">
                        </div>
                        <div class="field">
                            <label>Link twitter</label>
                            <input type="text" placeholder="" name="link_twitter"
                                   value="{{!empty($setting['link_twitter']) ? $setting['link_twitter'] : null}}">
                        </div>
                        <div class="field">
                            <label>Link youtube</label>
                            <input type="text" placeholder="" name="link_youtube"
                                   value="{{!empty($setting['link_youtube']) ? $setting['link_youtube'] : null}}">
                        </div>
                        <div class="field">
                            <label>Link google</label>
                            <input type="text" placeholder="" name="link_google"
                                   value="{{!empty($setting['link_google']) ? $setting['link_google'] : null}}">
                        </div>
                    </div>
                </div>
            </div>
            <br/>
            <div align="center">
                <button class="ui button primary" type="submit">บันทึก</button>
            </div>
        </form>
    </div>
@endsection

@section('sc-admin')

@endsection