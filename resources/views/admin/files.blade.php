@extends('admin.layout')

@section('header')
    <link rel="stylesheet" href="{{asset('plugin/dropzone/dropzone.css')}}">
@endsection

@section('content-admin')
    <div id="new_post">
        <div class="ui segment">
            <h3 class="ui dividing header header-h-new">
                จัดการไฟล์
            </h3>
            <div align="row">
                <div class="column">
                    <button class="ui primary basic button" onclick="uploadTheFile(true)" type="button">Uploads Files
                    </button>
                </div>
            </div>
            <br/>
            <div class="row">
                <div class="ui three column grid">
                    @foreach($data->list_files as $value)
                        <div class="column" id="file_id_{{$value->id}}">
                            <div class="ui fluid card">
                                <div class="image">
                                    <div class="card-thumb"
                                         style="background: url('{{fileConvertUrl($value,true)}}')"></div>
                                </div>
                                <div class="content box-content-file" align="center">
                                    <p style="text-align: left;height: 20px;overflow: hidden">{{$value->old_name}}</p>
                                    <a href="javascript:void(0)"><i
                                                onclick="copyToClipboard('{{fileConvertUrlLink($value)}}')"
                                                class="linkify icon"></i></a>
                                    <a href="javascript:void(0)"><i onclick="removeFile({{$value->id}})"
                                                                    class="trash icon"></i></a>
                                </div>
                            </div>
                        </div>
                    @endforeach
                </div>
            </div>
            <br/>
            <p>พบ {{$data->list_files->total()}} ไฟล์</p>
            @if($data->list_files->count() > 0)
                <br/>
                <div align="center">
                    {{$data->list_files->links()}}
                </div>
            @endif
        </div>
    </div>
@endsection

@section('sc')
    <script src="{{asset('plugin/dropzone/dropzone.js')}}"></script>
@endsection