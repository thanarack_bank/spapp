@extends('admin.layout')

@section('content-admin')
    <div class="ui three cards doubling dh-card">
        <div class="card">
            <div class="content">
                <div class="header">สมาชิกทั้งหมด</div>
                <div class="description">
                    <p class="number-counter">
                        {{$data->member}}
                    </p>
                    <div align="center">
                        <a class="fluid ui button basic primary" href="{{url('admin/member')}}">จัดการสมาชิก</a>
                    </div>
                </div>
            </div>
        </div>
        <div class="card">
            <div class="content">
                <div class="header">โพสทั้งหมด</div>
                <div class="description">
                    <p class="number-counter">
                        {{$data->posts}}
                    </p>
                    <div align="center">
                        <a class="fluid ui button basic primary" href="{{url('admin/post/list')}}">รายการโพส</a>
                    </div>
                </div>
            </div>
        </div>
        <div class="card">
            <div class="content">
                <div class="header">แบบทดสอบทั้งหมด</div>
                <div class="description">
                    <p class="number-counter">
                        {{$data->quiz}}
                    </p>
                    <div align="center">
                        <a class="fluid ui button basic primary" href="{{url('admin/quiz/list')}}">รายการแบบทดสอบ</a>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection