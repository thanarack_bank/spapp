@extends('admin.layout')

@section('content-admin')
    <div id="settings">

        <form class="ui form" method="post" enctype="multipart/form-data">
            {{csrf_field()}}
            <div class="ui segment">
                <h3 class="ui dividing header header-h-new">
                    ตั้งค่า - ทั่วไป
                </h3>
                <div class="ui doubling grid">
                    <div class=" column">
                        <div class="field">
                            <label>Icon</label>
                            <img src="{{!empty($setting['icon_web']) ? $setting['icon_web'] : null}}" class="icon-exam"
                                 id="icon-exam">
                            <button type="button" class="ui button grey mini"
                                    onclick="document.getElementById('icon-web').click();">
                                Upload
                            </button>
                            <input type="file" class="ui small" name="icon_web" id="icon-web" style="display: none"
                                   onchange='openFile(event,"icon-exam")'
                                   name="icon">
                        </div>
                        <div class="field">
                            <label>ชื่อเว็บไชต์</label>
                            <input type="text" placeholder="" name="web_title"
                                   value="{{!empty($setting['web_title']) ? $setting['web_title'] : null}}">
                        </div>
                        <div class="field">
                            <label>คำอธิบายเว็บไชต์</label>
                            <textarea rows="2"
                                      name="web_desc">{{!empty($setting['web_desc']) ? $setting['web_desc'] : null}}</textarea>
                        </div>
                        <div class="field">
                            <label>ไตเติ้ลข้อสอบ</label>
                            <input type="text" placeholder="" name="quiz_title"
                                   value="{{!empty($setting['quiz_title']) ? $setting['quiz_title'] : null}}">
                        </div>
                        <div class="field">
                            <label>คำอธิบายข้อสอบ</label>
                            <textarea rows="2"
                                      name="quiz_desc">{{!empty($setting['quiz_desc']) ? $setting['quiz_desc'] : null}}</textarea>
                        </div>
                    </div>
                </div>
            </div>
            <div class="ui segment">
                <div class="field">
                    <div class="ui checkbox enable">
                        <input type="checkbox" tabindex="0"
                               class="hidden" {{$setting['enable_member'] == 'on' ? 'checked="checked"' : null}} >
                        <input type="hidden" class="hidden" name="enable_member" id="" required
                               value="{{$setting['enable_member'] == 'on' ? 'on' : 'off'}}">
                        <label>เปิดใช้งานสมัครสมาชิก</label>
                    </div>
                </div>
            </div>
            <div class="ui segment">
                <div class="field">
                    <label>ขนาดความกว้าง (Thumb)</label>
                    <input type="number" placeholder="" name="img_cover_wide"
                           value="{{!empty($setting['img_cover_wide']) ? $setting['img_cover_wide'] : null}}">
                </div>
                <div class="field">
                    <label>ขนาดความสูงหน้า(Thumb)</label>
                    <input type="number" placeholder="" name="img_cover_height"
                           value="{{!empty($setting['img_cover_height']) ? $setting['img_cover_height'] : null}}">
                </div>
            </div>
            <div class="ui segment">
                <div class="ui doubling grid">
                    <div class=" column">
                        {{csrf_field()}}
                        <div class="field">
                            <label>อีเมล์เว็บ</label>
                            <input type="text" placeholder="" name="email"
                                   value="{{!empty($setting['email']) ? $setting['email'] : null}}">
                        </div>
                        <div class="field">
                            <label>ไลน์ไอดี</label>
                            <textarea rows="2"
                                      name="line">{{!empty($setting['line']) ? $setting['line'] : null}}</textarea>
                        </div>
                        <div class="field">
                            <label>เบอร์ติดต่อ</label>
                            <input type="text" placeholder="" name="tel"
                                   value="{{!empty($setting['tel']) ? $setting['tel'] : null}}">
                        </div>
                        <div class="field">
                            <label>ข้อความเกี่ยวกับเรา</label>
                            <textarea rows="2"
                                      name="about_footer">{{!empty($setting['about_footer']) ? $setting['about_footer'] : null}}</textarea>
                        </div>
                    </div>
                </div>
            </div>
            <br/>
            <div align="center">
                <button class="ui button primary" type="submit">บันทึก</button>
            </div>
        </form>
    </div>
@endsection

@section('sc-admin')

@endsection