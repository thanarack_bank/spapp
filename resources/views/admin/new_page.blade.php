@extends('admin.layout')

@section('header')

@endsection

@section('content-admin')
    <div id="new_post">
        <div class="ui segment">
            <h3 class="ui dividing header header-h-new">
                แก้ไขโพส : {{$data->post->title}}
            </h3>

            <div class="ui doubling grid">
                <div class="column">
                    <form class="ui form" method="post" action="{{url('admin/page/save')}}"
                          enctype="multipart/form-data">
                        {{csrf_field()}}
                        @if(isset($data->post))
                            <input type="hidden" name="id_update" value="{{$data->post->id}}">
                        @endif
                        <div class="field">
                            <label>หัวข้อ / ชื่อเรื่อง</label>
                            <input type="text" name="title" placeholder=""
                                   value="{{isset($data->post) ? $data->post->title:null}}" required maxlength="200">
                        </div>
                        <div class="field">
                            <label>รายละเอียด</label>
                            <p>
                                <button class="ui tiny primary basic button" type="button"
                                        onclick="filesSelect('image','1','textarea','content-post')">เพิ่มรูป</button>
                                <button class="ui tiny primary basic button" type="button"
                                        onclick="filesSelect('files','1','textarea','content-post')">
                                    เพิ่มไฟล์แนบ
                                </button>
                                <button class="ui tiny primary basic button" onclick="uploadTheFile(false)" type="button">
                                    อัพโหลดไฟล์
                                </button>
                            </p>
                            <textarea name="content"
                                      id="content-post">{{isset($data->post) ? $data->post->content:null}}</textarea>
                        </div>
                        <br/>
                        <div align="right">
                            <button class="ui button large primary" type="submit">บันทึก</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('sc-admin')
    <script src="{{asset('tinymce/js/tinymce/tinymce.min.js')}}"></script>
    <script src="{{asset('tinymce/setup.js?v=1')}}"></script>
    <script src="{{asset('plugin/dropzone/dropzone.js')}}"></script>
@endsection