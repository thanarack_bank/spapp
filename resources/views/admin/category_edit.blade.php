@extends('admin.layout')


@section('content-admin')
    <div id="new_post">
        <div class="ui segment">
            <h3 class="ui dividing header header-h-new">
                @if(isset($data->post))
                    แก้ไข : {{$data->post->title}}
                @else
                    เพิ่มหมวดหมู่
                @endif
            </h3>

            <div class="ui doubling grid">
                <div class="column">
                    <form class="ui form" method="post"
                          enctype="multipart/form-data">
                        {{csrf_field()}}
                        @if(isset($data->post))
                            <input type="hidden" name="id_update" value="{{$data->post->id}}">
                        @endif
                        <div class="field" align="center">
                            <div class="inline fields">
                                <label for="fruit">สถานะโพส :</label>
                                <div class="field">
                                    <div class="ui radio checkbox">
                                        <input type="radio" name="active"
                                               value="no"
                                               {{isset($data->post) && $data->post->active == 'no' ? 'checked':(empty($data->post ) ? 'checked' :null)}}
                                               tabindex="0"
                                               class="hidden">
                                        <label>แบบร่าง</label>
                                    </div>
                                </div>
                                <div class="field">
                                    <div class="ui radio checkbox">
                                        <input type="radio" name="active"
                                               {{isset($data->post) && $data->post->active == 'yes' ? 'checked':''}}
                                               value="yes"
                                               tabindex="0" class="hidden">
                                        <label>สาธารณะ</label>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="field">
                            <label>หัวข้อ / ชื่อเรื่อง</label>
                            <input type="text" name="title" placeholder=""
                                   value="{{isset($data->post) ? $data->post->title:null}}" required maxlength="200">
                        </div>
                        <div class="field">
                            <label>Url Slug</label>
                            <input type="text" name="slug" placeholder=""
                                   value="{{isset($data->post) ? $data->post->slug:null}}" required maxlength="200">
                        </div>
                        <div class="field">
                            <label>หน่วยงาน</label>
                            <select class="ui search dropdown" name="type">
                                <option {{isset($data->post) && 'org' == $data->post->type ? 'selected':null}} value="org">
                                    หน่วยงาน
                                </option>
                                <option {{isset($data->post) && 'tag' == $data->post->type ? 'selected':null}} value="tag">
                                    แท็ค/คำ
                                </option>
                                <option {{isset($data->post) && 'category' == $data->post->type ? 'selected':null}} value="category">
                                    เมนูเว็บ
                                </option>
                                <option {{isset($data->post) && 'quiz' == $data->post->type ? 'selected':null}} value="quiz">
                                    เมนูข้อสอบ
                                </option>
                            </select>
                        </div>

                        <br/>
                        <div align="right">
                            <button class="ui button large primary" type="submit">บันทึก</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
@endsection
