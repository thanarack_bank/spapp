@extends('admin.layout')

@section('content-admin')
    <div id="settings">

        <form class="ui form" method="post" action="{{url('admin/settings')}}" enctype="multipart/form-data">
            <div class="ui segment">
                <h3 class="ui dividing header header-h-new">
                    ตั้งค่า - โซเซียล
                </h3>

                <div class="ui doubling grid">
                    <div class=" column">
                        {{csrf_field()}}
                        <div class="field">
                            <label>โฆษณา ประกาศข่าว ตำแหน่ง 1</label>
                            <textarea name="ads_post_01">{{!empty($setting['ads_post_01']) ? $setting['ads_post_01'] : null}}</textarea>
                        </div>
                        <div class="field">
                            <label>โฆษณา ประกาศข่าว ตำแหน่ง 2</label>
                            <textarea name="ads_post_02">{{!empty($setting['ads_post_02']) ? $setting['ads_post_02'] : null}}</textarea>
                        </div>
                        <div class="field">
                            <label>โฆษณา ทำข้อสอบ ตำแหน่ง 1</label>
                            <textarea name="ads_quiz_01">{{!empty($setting['ads_quiz_01']) ? $setting['ads_quiz_01'] : null}}</textarea>
                        </div>
                        <div class="field">
                            <label>โฆษณา ทำข้อสอบ ตำแหน่ง 2</label>
                            <textarea name="ads_quiz_02">{{!empty($setting['ads_quiz_02']) ? $setting['ads_quiz_02'] : null}}</textarea>
                        </div>
                    </div>
                </div>
            </div>
            <br/>
            <div align="center">
                <button class="ui button primary" type="submit">บันทึก</button>
            </div>
        </form>
    </div>
@endsection

@section('sc-admin')

@endsection