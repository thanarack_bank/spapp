@extends('admin.layout')

@section('content-admin')
    <div id="new_post">
        <div class="ui segment">
            <h3 class="ui dividing header header-h-new">
                แบบทดสอบทั้งหมด
            </h3>

            <div class="ui doubling grid">
                <div class="column">
                    <div>
                        <table class="ui striped table">
                            <thead>
                            <tr>
                                <th width="25%">Name</th>
                                <th width="25%">Email</th>
                                <th width="20%">Create Date</th>
                                <th width="20%">Active</th>
                                <th width="" align="center">Action</th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach($data->list_member as $value)
                                <tr id="id_member_{{$value->id}}">
                                    <td>{{$value->name}}</td>
                                    <td>{{$value->email}}</td>
                                    <td>{{$value->created_at}}</td>
                                    <td>
                                        <span class="ui {{$value->active == 'no' ? 'red' : 'green'}} circular label">{{ucfirst($value->active)}}</span>
                                    </td>
                                    <td>
                                        <button class="ui mini icon {{$value->active == 'no' ? 'green' : 'orange'}}  button"
                                                onclick="lockMember('{{$value->id}}','{{$value->active}}')">
                                            <i class="lock icon"></i>
                                        </button>
                                        <button class="ui mini icon red  button" onclick="removeMember({{$value->id}})">
                                            <i class="trash icon"></i>
                                        </button>
                                    </td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                        <p>พบ {{number_format($data->list_member->total())}} แถว</p>
                        @if($data->list_member->count() > 0)
                            <div align="center">
                                {{$data->list_member->links()}}
                            </div>
                        @endif
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('sc-admin')
    <script>
        function removeMember(id) {
            if (!confirm('ต้องการลบสมาชิกคนนี้หรือไม่ ?')) return false;
            $.post(APP_URL + 'api/remove_member/' + id, {
                _token: APP_TOKEN
            }, function (res) {
                if (res.status === 100) {
                    $('#id_member_' + id).remove();
                }
            });
        }

        function lockMember(id, active) {
            if (active === 'no') {
                if (!confirm('ต้องการยกเลิกแบนผู้ใช้คนนี้ ?')) return false;
            } else {
                if (!confirm('ต้องการแบนผู้ใช้งานคนนี้ ?')) return false;
            }
            $.post(APP_URL + 'api/lock_member/' + id + '?active=' + active, {
                _token: APP_TOKEN
            }, function (res) {
                window.location.reload();
            });
        }
    </script>
@endsection