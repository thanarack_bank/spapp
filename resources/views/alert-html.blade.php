@if(session()->has('success-save'))
    <div class="ui success message box-msg transition">
        <i class="close icon"></i>
        <div class="header">
            สำเร็จ
        </div>
        <p>ระบบบันทึกข้อมูลของคุณแล้ว</p>
    </div>
@endif
@if($errors->any())
    @foreach($errors->all() as $error)
        <div class="ui error message box-msg transition">
            <i class="close icon"></i>
            <div class="header">
                {{$error}}
            </div>
        </div>
    @endforeach
@endif