@extends('layouts.app')

@section('content')
    <section id="finder">
        <div class="finder">
            <div class="ui container">
                <div class="t-i">
                    <div class="t-i-c">
                        <p class="font-pp">ค้นหางานราชการ<br/>หรืองานเอกชนที่เปิดสอบ</p>
                        <form method="get" action="{{url('search')}}">
                            <div class="ui big icon input">
                                <input type="text" placeholder="ค้นหา" name="q">
                                <i class="search icon"></i>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <section id="last_new">
        <div class="ui container">
            <h3 class="ui dividing header header-last-new">
                ข่าวเปิดสอบล่าสุด
            </h3>
            @if($data->last_post)
                <div class="ui four stackable cards">
                    @foreach($data->last_post as $key=>$value)
                        <div class="ui card">
                            <a class="image" href="{{url('view/'.$value->id)}}">
                                <div class="card-thumb" style="background: url('{{fileConvertUrl($value->files,true)}}')"></div>
                            </a>

                            <div class="content">
                                <a class="header title-post" href="{{url('view/'.$value->id)}}">{{$value->title}}</a>

                                <div class="meta">
                                    <p class="by">ที่มา
                                        : {{isset($value->org->title) ? $value->org->title : '-'}}</p>

                                    <p class="date">{{$value->updated_at->format('j M Y')}}</p>
                                </div>
                            </div>
                        </div>
                    @endforeach
                </div>
            @endif
        </div>
        <div class="ui container load-more" align="center">
            <a class="ui primary basic button" href="{{url('all')}}">ดูทั้งหมด</a>
        </div>
    </section>
@endsection