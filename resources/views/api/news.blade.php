@extends('layouts.if')

@section('content')
    <div class="container">
        <div class="row">
            @foreach($data->lists_post as $key=>$value)
                <div class="col-sm-6 col-md-4">
                    <div class="thumbnail if_thum">
                        <a href="{{url('view/'.$value->id)}}" target="_blank">
                            <div style="background: url('{{fileConvertUrl($value->files,true)}}');" class="if_bg"></div>
                        </a>
                        <div class="caption">
                            <h3><a  target="_blank" href="{{url('view/'.$value->id)}}">{{$value->title}}</a></h3>
                            <p class="if_date">{{$value->created_at->format('d/m/Y H:i')}}</p>
                            <p class="if_org">{{$value->org->title}}</p>
                        </div>
                        <div class="clearfix"></div>
                    </div>
                </div>
            @endforeach
        </div>
    </div>

    @if($data->lists_post->count() > 0)
        <div align="center">
            {{$data->lists_post->links()}}
        </div>
    @endif
@endsection