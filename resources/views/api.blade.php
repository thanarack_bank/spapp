@extends('layouts.app')

@section('content')
    <section id="content">
        <div class="ui container">
            <div class="ui segment">
                <h3 class="ui dividing header header-h-new">
                    ข่าวติดเว็บ
                </h3>
                <div style="min-height: 300px;">
                    <div class="ui form">
                        <div class="field">
                            <label>Copy วางที่เว็บไชต์ของคุณ</label>
                            <textarea class="" width="100%" rows="1" readonly><iframe src="{{url('api/news')}}" style="width: 100%;height: 350px;border: 1px solid #dededf"></iframe></textarea>
                        </div>
                        <div class="field">
                            <label>ตัวอย่าง</label>
                            <iframe src="{{url('api/news')}}" style="width: 100%;height: 350px;border: 1px solid #dededf"></iframe>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection