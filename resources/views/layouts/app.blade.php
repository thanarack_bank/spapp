<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>{{isset($data->title) ? $data->title : env('APP_NAME')}}</title>
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1"/>
    <meta name="viewport" content="width=device-width, initial-scale=1, minimum-scale=1, maximum-scale=1">
    <meta name="description" content="{{isset($data->des) ? $data->des : null}}">
    <link rel="icon" href="{{config_db('icon_web')}}">
    <link rel="canonical" href="{{Request::url()}}">
    <link rel="stylesheet" type="text/css" href="{{asset('Semantic-UI-CSS-master/semantic.min.css')}}">
    <link rel="stylesheet" type="text/css" href="{{asset('Semantic-UI-CSS-master/grid.min.css')}}">
    <link rel="stylesheet" type="text/css" href="{{asset('plugin/dropzone/dropzone.css')}}">
    <link rel="stylesheet" type="text/css" href="{{asset('css/app.css?v='.rand())}}">
    <link href="https://fonts.googleapis.com/css?family=Prompt:300,400,500" rel="stylesheet">
    <link rel="manifest" href="{{asset('manifest.json')}}">
</head>
<body class="{{(request()->is('admin/*') ? 'admin-body' : 'web-body')}}">

<script src="https://cdn.onesignal.com/sdks/OneSignalSDK.js" async></script>
<script>
    var OneSignal = window.OneSignal || [];
    OneSignal.push(["init", {
        setDefaultTitle: 'ติดตามเราเพื่อรับข่าวสาร',
        appId: "0210f609-624b-4bb6-802d-32834fbffabb",
        autoRegister: true,
        notifyButton: {
            enable: true /* Set to false to hide */,
            text: {
                'tip.state.unsubscribed': 'กดติดตามเพื่อรับแจ้งเตือนข่าวสารเปิดสอบ',
                'tip.state.subscribed': "ยกเลิกการติดตาม",
                'tip.state.blocked': "คุณได้บล็อคการแจ้งเตือน จะไม่ได้รับการแจ้งเตือนจากเราอีกต่อไป",
                'message.prenotify': 'กดเพื่อรับการแจ้งเตือน',
                'message.action.subscribed': "สำเร็จคุณได้ติดตามเราแล้ว",
                'message.action.resubscribed': "สำเร็จคุณได้ติดตามเราแล้ว",
                'message.action.unsubscribed': "คุณจะไม่ได้รับการแจ้งเตือนอีกต่อไป",
                'dialog.main.title': 'จัดการ การแจ้งเตือน',
                'dialog.main.button.subscribe': 'ติดตามเรา',
                'dialog.main.button.unsubscribe': 'ยกเลิกการติดตาม',
                'dialog.blocked.title': 'ยกเลิกการบล็อคแล้ว',
                'dialog.blocked.message': "คุณได้ติดตาม :"
            }
        }
    }]);
</script>
<style>
    .follow_title{
        position: fixed;
        bottom: 13px;
        right: 60px;
        width: 200px;
        color: #dd9191;
    }
</style>
<div id="modal-zone"></div>
<div id="modal-zone-pick"></div>
<script>
    var APP_URL = '{{env('APP_URL')}}';
    var APP_TOKEN = '{{csrf_token()}}';
</script>
<div class="page-wrap">
    @include('navBar')
    @yield('content')
</div>
<!--<p class="follow_title">กดรับการแจ้งเตือนข่าวเปิดสอบฟรี</p>-->
@include('footer')
<script src="https://code.jquery.com/jquery-3.1.1.min.js"
        integrity="sha256-hVVnYaiADRTO2PzUGmuLJr8BLUSjGIZsDYGmIJLv2b8="
        crossorigin="anonymous"></script>
<script src="{{asset('Semantic-UI-CSS-master/semantic.min.js')}}"></script>
<script>
    $(function () {
        $('.ui.dropdown').dropdown();
    });

    $('.launch').click(function(){
        $('.ui.labeled.icon.sidebar').sidebar('toggle');
    });
</script>
@yield('sc')
<script src="{{asset('js/appSetupBackend.js?v='.rand())}}"></script>
<script>
    function PopupCenter(url, title, w, h, mode) {
        var url_social;

        if (mode === 'facebook') {
            url_social = 'https://www.facebook.com/sharer/sharer.php?u=';
        }
        if (mode === 'google') {
            url_social = 'https://plus.google.com/share?url=';
        }
        if (mode === 'twitter') {
            url_social = 'https://twitter.com/home?status=';
        }

        var url = url_social + url;
        var dualScreenLeft = window.screenLeft != undefined ? window.screenLeft : screen.left;
        var dualScreenTop = window.screenTop != undefined ? window.screenTop : screen.top;

        var width = window.innerWidth ? window.innerWidth : document.documentElement.clientWidth ? document.documentElement.clientWidth : screen.width;
        var height = window.innerHeight ? window.innerHeight : document.documentElement.clientHeight ? document.documentElement.clientHeight : screen.height;

        var left = ((width / 2) - (w / 2)) + dualScreenLeft;
        var top = ((height / 2) - (h / 2)) + dualScreenTop;
        var newWindow = window.open(url, title, 'scrollbars=yes, width=' + w + ', height=' + h + ', top=' + top + ', left=' + left);

        // Puts focus on the newWindow
        if (window.focus) {
            newWindow.focus();
        }
    }
</script>
</body>
</html>