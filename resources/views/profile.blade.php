@extends('layouts.app')

@section('content')
    <section id="view_post">
        <div class="ui grid doubling stackable container">
            <div class="three wide column">
                @include('slide-member')
            </div>
            <div class="thirteen wide column">
                <div class="content-page-view">
                    @include('alert-html')
                    <div class="ui segment" style="margin-top: 0;">
                        <h3 class="ui dividing header header-h-new">
                            ข้อมูลส่วนตัว
                        </h3>
                        <div class="content">
                            <form class="ui form" method="post" enctype="multipart/form-data">
                                {{csrf_field()}}
                                <div class="field">
                                    <label>รูปประจำตัว</label>
                                    <img src="{{$data->user->user_info->files ? fileConvertUrl($data->user->user_info->files,true) : null}}"
                                         class="avatar-exam ui rounded image"
                                         id="avatar-show">
                                    <button type="button" class="ui button grey mini"
                                            onclick="document.getElementById('avatar').click();">
                                        Upload
                                    </button>
                                    <input type="file" class="ui small" name="file" id="avatar" style="display: none"
                                           onchange='openFile(event,"avatar-show")'
                                           name="icon">
                                </div>
                                <div class="field">
                                    <label>ชื่อ (แสดงผล)</label>
                                    <input type="text" name="name" placeholder="" value="{{$data->user->name}}">
                                </div>
                                <div class="field">
                                    <label>อีเมล์</label>
                                    <input type="text" disabled name="email" placeholder=""
                                           value="{{$data->user->email}}">
                                </div>
                                <div class="two fields">
                                    <div class="field">
                                        <label>เบอร์โทร</label>
                                        <input type="text" name="tel" placeholder="" maxlength="10"
                                               value="{{$data->user->user_info->tel}}">
                                    </div>
                                    <div class="field">
                                        <label>เพศ</label>
                                        <select class="ui fluid dropdown" name="sex">
                                            <option value="male" {{$data->user->user_info->sex == 'male' ? 'selected' : null}}>
                                                ชาย
                                            </option>
                                            <option value="female" {{$data->user->user_info->sex == 'female' ? 'selected' : null}}>
                                                หญิง
                                            </option>
                                        </select>
                                    </div>
                                </div>
                                <br/>
                                <div align="right">
                                    <button class="ui primary button" type="submit">อัพเดท</button>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection
