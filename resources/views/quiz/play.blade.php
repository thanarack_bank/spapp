@extends('layouts.app')

@section('content')
    <section id="view_post">
        <div class="ui grid  stackable container">
            <div class="three wide column">
                @include('quiz/slide')
            </div>
            <div class="thirteen wide column">
                <div class="content-page-view">
                    <div class="ui card-page-view card">
                        <div class="content">
                            <div class="header title-card"><h2 title="{{$data->post->title}}">{{$data->post->title}}</h2></div>
                            <div class="meta"><i class="calendar icon"></i> อัพเดตเมื่อ
                                : {{$data->post->updated_at->format('j M Y H:i')}}</div>
                            <div class="meta"><i class="eye icon"></i> ดู : {{$data->post->views}} ครั้ง</div>
                            <div class="meta"><i class="question icon"></i> คำถามทั้งหมด
                                : {{$data->list_question->count()}} ข้อ
                            </div>
                        </div>
                        <div class="extra content quiz-content">
                            {!! config_db('ads_quiz_01') !!}
                            @if($data->post->content)
                                <p class="meta-description-quiz">{{$data->post->content}}</p>
                                <br/>
                            @endif
                            @if(Auth::check())
                                <div align="center">
                                    <button class="ui button icon large green" type="button" onclick="playButton(this)">
                                        <i
                                                class="play icon"></i> เริ่มทำแบบทดสอบ
                                    </button>
                                    <div class="clock-counter">00:00:00</div>
                                </div>
                                <div id="quiz_content" style="display: none;">
                                    <form id="form_quiz" method="post">
                                        {{csrf_field()}}
                                        <input type="hidden" name="timer_count" id="timer_count" value="0">
                                        @foreach($data->list_question as $key=> $value)
                                            <input type="hidden" name="question[]" value="{{$value->id}}">
                                            <div class="question_box" id="id_quiz_{{$value->id}}">
                                                <table style="margin-bottom: 2em;" class="ui celled striped table">
                                                    <thead>
                                                    <tr>
                                                        <th colspan="2">ข้อ <span
                                                                    class="number-of-question">{{$key+1}}
                                                                - {{$value->title}}</span></th>
                                                    </tr>
                                                    </thead>
                                                    <tbody>
                                                    @foreach($value->answer_quiz as $key_2 => $value_2)
                                                        <tr>
                                                            <td style="text-align: center" width="25%">
                                                                <div class="field">
                                                                    <div class="ui radio checkbox">
                                                                        <input type="radio" required
                                                                               class="answer-true"
                                                                               value="{{$value_2->id}}"
                                                                               name="answer_true_{{$value->id}}"
                                                                               tabindex="0">
                                                                        <label style="color: #c3c3c3;">คำตอบที่
                                                                            <span>{{$key_2+1}}</span></label>
                                                                    </div>
                                                                </div>
                                                            </td>
                                                            <td>
                                                                <div class="ui small input">{{$value_2->title}}</div>
                                                            </td>
                                                        </tr>
                                                    @endforeach
                                                    </tbody>
                                                </table>
                                            </div>
                                        @endforeach
                                        <p align="center">
                                            <button class="ui icon button green" id="submit_quiz" type="button"
                                                    onclick="call_submit()">
                                                <i class="send icon"></i>
                                                ส่งคำตอบ
                                            </button>
                                            <button type="submit" class="hide" id="submit_btn"></button>
                                        </p>
                                    </form>
                                </div>
                            @else
                                <h4>กรุณาเข้าสู่ระบบก่อนเล่น</h4>
                                <a href="{{url('login?red='.getMyPage())}}"
                                   class="ui button basic primary ">เข้าสู่ระบบ</a>
                            @endif
                            {!! config_db('ads_quiz_02') !!}
                        </div>
                        <div class="extra content  shared-log" align="center">
                            <p class="txt-s"><i class="share icon"></i> แชร์แบบทดสอบ</p>
                            <button class="ui facebook button"
                                    onclick="PopupCenter('{{Request::url()}}','{{$data->title}}','780','450','facebook')">
                                <i class="facebook icon"></i>
                                Facebook
                            </button>
                            <button class="ui twitter button"
                                    onclick="PopupCenter('{{Request::url()}}','{{$data->title}}','780','450','twitter')">
                                <i class="twitter icon"></i>
                                Twitter
                            </button>
                            <button class="ui google plus button"
                                    onclick="PopupCenter('{{Request::url()}}','{{$data->title}}','780','450','google')">
                                <i class="google plus icon"></i>
                                Google Plus
                            </button>
                        </div>
                        <div class="extra content">
                         <p class="post-tags"><strong>หมวดหมู่:</strong>
                                @foreach($data->post->cate_relation as $key=>$value)
                                    @if($value->cate)
                                        <a class="ui blue small tag label"
                                           href="{{url('quiz/category/'.$value->cate->slug)}}">{{$value->cate->title}}</a>
                                    @endif
                                @endforeach
                            </p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <div class="ui basic modal modal-confirm">
        <div class="ui icon header">
            <i class="check icon"></i>
            ยืนยันส่งคำตอบนี้
        </div>
        <div class="actions center-submit ">
            <div class="ui red basic cancel inverted button">
                <i class="remove icon"></i>
                ไม่ใช่ตอนนี้
            </div>
            <div class="ui green ok inverted button" onclick="submit_quiz_play()">
                <i class="checkmark icon"></i>
                ส่งคำตอบ
            </div>
        </div>
    </div>
@endsection

@section('sc')
    <script src="{{asset('plugin/jquery.countdown-2.2.0/jquery.countdown.min.js')}}"></script>
    <script>
        $(function () {
            $('#submit_btn').hide();
            $('.extra.content.description img').addClass('ui centered medium image');
        });

        var playStart = 0;
        var timeStampPlay = '';

        function playButton(event) {
            if (playStart == 1) return false;

            playStart = 1;

            $('#quiz_content').slideDown();
            var fiveSeconds = new Date().getTime();
            $('.clock-counter').countdown(fiveSeconds, {elapse: true})
                .on('update.countdown', function (event) {
                    var $this = $(this);
                    if (event.elapsed) {
                        $this.html(event.strftime('<span>%H:%M:%S</span>'));
                        timeStampPlay = event.strftime('%H:%M:%S');
                    } else {
                        $this.html(event.strftime('<span>%H:%M:%S</span>'));
                    }

                });
            $(event).prop('disabled', true);
        }

        function call_submit() {
            if (playStart === 0) return false;
            $('#timer_count').val(timeStampPlay);
            $('.modal-confirm').modal({
                closable: false
            }).modal('show');
        }

        function submit_quiz_play() {
            if (playStart === 0) return false;
            $('#submit_btn').trigger('click');
        }
    </script>
@endsection