@extends('layouts.app')

@section('content')
    <section id="view_post">
        <div class="ui grid  stackable container">
            <div class="three wide column">
                @include('quiz/slide')
            </div>
            <div class="thirteen wide column">
                <div class="content-page-view">
                    @if(isset($data->last_post))
                        <h3 class="ui dividing header header-last-new">
                            แบบทดสอบล่าสุด
                            <a class="ui button grey small right floated"
                               href="{{url('quiz/list')}}">ดูเพิ่มเติม</a>
                            <div class="sub header">อัพเดทข้อสอบมาใหม่</div>
                        </h3>
                        <div class="ui three stackable cards">
                            @foreach($data->last_post as $key=>$value)
                                <div class="ui card">
                                    <a class="image" href="{{url('quiz/play/'.$value->id)}}">
                                        <div class="card-thumb" style="background: url('{{fileConvertUrl($value->files,true)}}')"></div>
                                    </a>

                                    <div class="content">
                                        <a class="header title-post"
                                           href="{{url('quiz/play/'.$value->id)}}">{{$value->title}}</a>

                                        <div class="meta">
                                            <p class="view">ดูแล้ว: {{$value->views}} ครั้ง</p>
                                            <p class="date">สร้างเมื่อ: {{$value->updated_at->format('j M Y')}}</p>
                                        </div>
                                        <div class="button-z">
                                            <a class="ui teal button basic small"  href="{{url('quiz/play/'.$value->id)}}"><i class="play icon"></i> ทำข้อสอบ</a>
                                        </div>
                                    </div>
                                </div>
                            @endforeach
                        </div>
                    @endif

                    @if(isset($data->quiz_menu))
                        @foreach($data->quiz_menu as $key=> $value)
                            <br/><br/>
                            <h3 class="ui dividing header header-last-new">
                                {{$data->cate_detail[$key]->title}}
                                <a class="ui button grey small right floated"
                                   href="{{url('quiz/category/'.$data->cate_detail[$key]->slug)}}">ดูเพิ่มเติม</a>
                                <div class="sub header">อัพเดทข้อสอบมาใหม่</div>
                            </h3>
                            <div class="ui three stackable cards">
                                @foreach($value as $key=>$value)
                                    <div class="ui card">
                                        <a class="image" href="{{url('quiz/play/'.$value->id)}}">
                                            <div class="card-thumb" style="background: url('{{fileConvertUrl($value->files,true)}}')"></div>
                                        </a>

                                        <div class="content">
                                            <a class="header title-post"
                                               href="{{url('quiz/play/'.$value->id)}}">{{$value->title}}</a>

                                            <div class="meta">
                                                <p class="view">ดูแล้ว: {{$value->views}} ครั้ง</p>
                                                <p class="date">สร้างเมื่อ: {{$value->updated_at->format('j M Y')}}</p>
                                            </div>
                                            <div class="button-z">
                                                <a class="ui teal button basic small"  href="{{url('quiz/play/'.$value->id)}}"><i class="play icon"></i> ทำข้อสอบ</a>
                                            </div>
                                        </div>
                                    </div>
                                @endforeach
                            </div>
                        @endforeach
                    @endif

                </div>
            </div>
        </div>
    </section>
@endsection