@extends('layouts.app')

@section('content')
    <section id="view_post">
        <div class="ui grid  stackable container">
            <div class="three wide column">
                @include('quiz/slide')
            </div>
            <div class="thirteen wide column">
                <div class="content-page-view">
                    <div class="ui segment">
                        <h3 class="ui dividing header header-h-new">
                            ประวัติทำข้อสอบ
                        </h3>
                        <div class="content">
                            <table class="ui table">
                                <thead>
                                <th width="20%">วันที่</th>
                                <th>หัวข้อ</th>
                                <th>คะแนน</th>
                                <th width="5%"></th>
                                </thead>
                                <tbody>
                                @foreach($data->list_history as $key=>$value)
                                    <tr>
                                        <td>{{$value->created_at}}</td>
                                        <td>{{($value->post) ? $value->post->title : 'ไม่พบหัวข้อ'}}</td>
                                        <td>{{$value->score}}</td>
                                        <td align="center">
                                        @if($value->post)
                                        	<a href="{{url('quiz/result/'.$value->id_key)}}"><i
                                                        class="wpforms icon"></i></a>
                                        @endif
                                                        </td>
                                    </tr>
                                @endforeach
                                </tbody>
                            </table>
                            <p>พบ {{number_format($data->list_history->total())}} แถว</p>
                            @if($data->list_history->count() > 0)
                                <div align="center">
                                    {{$data->list_history->links()}}
                                </div>
                            @endif
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection
