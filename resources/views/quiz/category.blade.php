@extends('layouts.app')

@section('content')
    <section id="view_post">
        <div class="ui grid  stackable container">
            <div class="three wide column">
                @include('quiz/slide')
            </div>
            <div class="thirteen wide column">
                <div class="content-page-view">
                    @if(isset($data->last_post))
                        <h3 class="ui dividing header header-last-new">
                            {{$data->cate->title}}
                            <div class="sub header">อัพเดทข้อสอบมาใหม่</div>
                        </h3>
                        <div class="ui three stackable cards">
                            @foreach($data->last_post as $key=>$value)
                                <div class="ui card">
                                    <a class="image" href="{{url('quiz/play/'.$value->id)}}">
                                        <div class="card-thumb" style="background: url('{{fileConvertUrl($value->files,true)}}')"></div>
                                    </a>

                                    <div class="content">
                                        <a class="header title-post"
                                           href="{{url('quiz/play/'.$value->id)}}">{{$value->title}}</a>

                                        <div class="meta">
                                            <p class="view">ดูแล้ว: {{$value->views}} ครั้ง</p>
                                            <p class="date">สร้างเมื่อ: {{$value->updated_at->format('j M Y')}}</p>
                                        </div>
                                    </div>
                                </div>
                            @endforeach
                        </div>
                    @endif
                    @if($data->last_post->count()>0)
                        <div class="page-zone">
                            {{ $data->last_post->links('vendor/pagination/default') }}
                        </div>
                    @endif
                </div>
            </div>
        </div>
    </section>
@endsection