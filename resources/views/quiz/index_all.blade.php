@extends('layouts.app')

@section('content')
    <section id="view_post">
        <div class="ui grid  stackable container">
            <div class="three wide column">
                @include('quiz/slide')
            </div>
            <div class="thirteen wide column">
                <div class="content-page-view">
                    @if(isset($data->last_post))
                        <h3 class="ui dividing header header-last-new">
                            แบบทดสอบล่าสุด
                        </h3>
                        <div class="ui selection dropdown">
                            <input type="hidden" name="gender">
                            <i class="dropdown icon"></i>
                            <div class="{{ !Request::input('order') ? 'default' : null }} text">{{ Request::input('order') === '1' ? 'วันที่ มาก > น้อย' : 'วันที่ น้อย > มาก' }}</div>
                            <div class="menu">
                                <div onclick="new_order(1);" class="item {{ Request::input('order') === '1' ? 'active selected' : null }}" data-value="1">วันที่ มาก > น้อย</div>
                                <div onclick="new_order(2);" class="item {{ Request::input('order') === '2' ? 'active selected' : null }}" data-value="2">วันที่ น้อย > มาก</div>
                            </div>
                        </div>
                        <br/><br/>
                        <div class="ui three stackable cards">
                            @foreach($data->last_post as $key=>$value)
                                <div class="ui card">
                                    <a class="image" href="{{url('quiz/play/'.$value->id)}}">
                                        <div class="card-thumb"
                                             style="background: url('{{fileConvertUrl($value->files,true)}}')"></div>
                                    </a>

                                    <div class="content">
                                        <a class="header title-post"
                                           href="{{url('quiz/play/'.$value->id)}}">{{$value->title}}</a>

                                        <div class="meta">
                                            <p class="view">ดูแล้ว: {{$value->views}} ครั้ง</p>
                                            <p class="date">สร้างเมื่อ: {{$value->updated_at->format('j M Y')}}</p>
                                        </div>
                                        <div class="button-z">
                                            <a class="ui teal button basic small"
                                               href="{{url('quiz/play/'.$value->id)}}"><i class="play icon"></i>
                                                ทำข้อสอบ</a>
                                        </div>
                                    </div>
                                </div>
                            @endforeach
                        </div>
                    @endif
                    @if($data->last_post->count()>0)
                        <div class="page-zone">
                            {{ $data->last_post->links('vendor/pagination/default') }}
                        </div>
                    @endif
                </div>
            </div>
        </div>
    </section>
@endsection

@section('sc')
<script>
    function new_order(number){
        var url = APP_URL + 'quiz/list?page=1&order='+number;
        window.location.href = url;
    }
</script>
@endsection