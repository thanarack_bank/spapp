@extends('layouts.app')

@section('content')
    <section id="view_post">
        <div class="ui grid  stackable container">
            <div class="three wide column">
                @include('quiz/slide')
            </div>
            <div class="thirteen wide column">
                <div class="content-page-view">
                    <div class="ui card-page-view card result_log_box">
                        <div class="content">
                            <div class="header result_log icon"><i class="file text outline icon"></i></div>
                            <div class="header result_log">ผลการทดสอบ</div>
                            <div class="result_log_name">
                                <p><img src="{{fileConvertUrl($data->post->post->files,true)}}" class="ui medium rounded image"/></p>
                                <p>{{$data->post->post->title}}</p>
                            </div>
                            <br/>
                            <div class="ui centered card">
                                <div class="content" align="center">
                                    <strong>คะแนนที่ทำได้</strong>
                                </div>
                                <div class="content " align="center">
                                    <p class="score_log">{{$data->score}}</p>
                                </div>
                            </div>
                            <div align="center" class="ui centered card box-detail-log">
                                <table class="ui table no-border ">
                                    <tbody>
                                    <tr>
                                        <td class="td-title-log"><i class="angle right icon"></i> วันที่ทดสอบ</td>
                                        <td>{{$data->post->created_at}}</td>
                                    </tr>
                                    <tr>
                                        <td class="td-title-log"><i class="angle right icon"></i> ชื่อผู้ทดสอบ</td>
                                        <td>{{$data->post->post->user->name}}</td>
                                    </tr>
                                    <tr>
                                        <td class="td-title-log"><i class="angle right icon"></i> จำนวนข้อที่ผิด</td>
                                        <td>{{$data->post->quiz_result_log->count() - $data->score}}</td>
                                    </tr>
                                    <tr>
                                        <td class="td-title-log"><i class="angle right icon"></i> ใช้เวลาไป</td>
                                        <td>{{$data->post->timer}}</td>
                                    </tr>
                                    <tr>
                                        <td class="td-title-log"><i class="angle right icon"></i> ข้อสอบทั้งหมด</td>
                                        <td>{{$data->post->quiz_result_log->count()}}</td>
                                    </tr>
                                    </tbody>
                                </table>
                            </div>
                            <div class="shared-log" align="center">
                                <p class="txt-s"><i class="share icon"></i> แชร์ผลการทดสอบ</p>
                                <button class="ui facebook button"
                                        onclick="PopupCenter('{{Request::url()}}','{{$data->post->post->title}}','780','450','facebook')">
                                    <i class="facebook icon"></i>
                                    Facebook
                                </button>
                                <button class="ui twitter button"
                                        onclick="PopupCenter('{{Request::url()}}','{{$data->post->post->title}}','780','450','twitter')">
                                    <i class="twitter icon"></i>
                                    Twitter
                                </button>
                                <button class="ui google plus button"
                                        onclick="PopupCenter('{{Request::url()}}','{{$data->post->post->title}}','780','450','google')">
                                    <i class="google plus icon"></i>
                                    Google Plus
                                </button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection
