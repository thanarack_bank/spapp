@extends('layouts.app')

@section('content')
    <section id="view_post">
        <div class="ui grid  stackable container">
            <div class="three wide column">
                @include('quiz/slide')
            </div>
            <div class="thirteen wide column">
                <div class="content-page-view">
                    <div class="ui two cards doubling dh-card">
                        <div class="card">
                            <div class="content">
                                <div class="header">จำนวนผู้เล่น</div>
                                <div class="description">
                                    <p class="number-counter-max">
                                        {{number_format($data->total_player)}}
                                    </p>
                                </div>
                            </div>
                        </div>
                        <div class="card">
                            <div class="content">
                                <div class="header">จำนวนข้อสอบ</div>
                                <div class="description">
                                    <p class="number-counter-max">
                                        {{number_format($data->total_quiz)}}
                                    </p>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="ui segment">
                        <div class="content">
                            <table class="ui celled striped table">
                                <thead>
                                <th width="5%" class="center aligned">อันดับ</th>
                                <th width="5%"  class="center aligned">Avatar</th>
                                <th width="50%"  class="center aligned">ชื่อผู้เล่น</th>
                                <th width="10%"  class="center aligned">คะแนน</th>
                                </thead>
                                <tbody>
                                @foreach($data->list_rank as $key=>$value)
                                    <tr>
                                        <td  class="center aligned"><label class="box-show-res">อันดับ : </label>{{$key+1}}</td>
                                        <td class="center aligned">
                                        <div align="center"><img class="ui circular image" style="width: 42px;height: 42px;"
                                                      src="{{fileConvertUrl($value->files,true)}}"/></div>
                                        </td>
                                        <td  class="center aligned">{{$value->user->name}}</td>
                                        <td  class="center aligned"><label class="box-show-res">คะแนน : </label>{{number_format($value->total_score_quiz)}}</td>
                                    </tr>
                                @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection
