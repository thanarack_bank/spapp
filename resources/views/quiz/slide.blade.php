<div class="ui vertical menu">
    <a class="teal item" href="{{url('quiz/rank')}}"><i class="trophy icon"></i> อันดับคะแนน</a>
</div>

<div class="ui vertical menu">
    <div class="item">
        <form class="ui transparent icon input" action="{{url('search')}}" method="get">
            <input type="text" placeholder="ค้นหาข้อสอบออนไลน์" name="q">
            <i class="search icon"></i>
        </form>
    </div>
    @foreach(getListCate('quiz') as $key=>$value)
        <a class="teal item" href="{{url('quiz/category/'.$value->slug)}}">
            {{$value->title}}
        </a>
    @endforeach
</div>