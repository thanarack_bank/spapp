@extends('layouts.app')

@section('content')
    <section id="content">
        <div class="ui container">
            <div class="ui segment">
                <h3 class="ui dividing header header-h-new">
                    {{$data->about->title}}
                </h3>
                <div style="min-height: 300px;">
                    {!! htmlspecialchars_decode($data->about->content) !!}
                </div>
            </div>
        </div>
    </section>
@endsection