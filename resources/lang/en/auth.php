<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Authentication Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used during authentication for various
    | messages that we need to display to the user. You are free to modify
    | these language lines according to your application's requirements.
    |
    */

    'failed' => 'ไม่พบชื่อหรือบัญชีนี้ในระบบ.',
    'throttle' => 'Too many login attempts. Please try again in :seconds seconds.',
    'register_fails' => 'ไม่สามารถสมัครสมาชิกได้',
    'login_fails' => 'ไม่สามารถเข้าสู่ระบบได้'

];
