<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


// Admin Route
Route::group(['prefix' => 'admin', 'middleware' => ['admin.auth', 'front_end']], function () {
    Route::get('dashboard', 'Admin\Dashboard@index');
    Route::get('post/new', 'Admin\Post@new_post');
    Route::get('post/list', 'Admin\Post@list_post');
    Route::post('post/new', 'Admin\Post@save_post');
    Route::get('post/upload_file', 'Admin\Post@uploadFile');
    Route::post('post/upload_file', 'Admin\Post@uploadFile');
    Route::get('settings', 'Admin\Settings@index');
    Route::get('settings/social', 'Admin\Settings@social');
    Route::get('settings/ads', 'Admin\Settings@ads');
    Route::post('settings', 'Admin\Settings@save_settings');
    Route::get('member', 'Admin\Member@index');
    Route::get('category/new', 'Admin\CateController@category_edit');
    Route::post('category/new', 'Admin\CateController@category_edit_save');
    Route::get('category/{name}', 'Admin\CateController@manger');
    Route::get('quiz/list', 'Admin\Quiz@index');
    Route::get('files', 'FilesController@index');
    Route::get('page/{slug}', 'Admin\Post@new_page');
    Route::post('page/save', 'Admin\Post@save_page');
    Route::post('remove_file/{id}', 'FilesController@remove_file');
});

// API Route
Route::group(['prefix' => 'api'], function () {
    Route::post('remove_post/{id}', 'Admin\Post@remove_post');
    Route::post('remove_member/{id}', 'Admin\Member@remove_member');
    Route::post('remove_cate/{id}', 'Admin\CateController@remove_cate');
    Route::post('lock_member/{id}', 'Admin\Member@member_ban');
    Route::get('files_lists', 'FilesController@files_lists');
    Route::get('news', 'ApiController@index');
});


// Web Route
Route::group(['middleware' => 'front_end'], function () {
    Auth::routes();
    Route::get('/', 'HomeController@index')->name('home');
    Route::get('view/{id}', 'PostsController@view_post')->where('id', '[0-9]+');
    Route::get('search', 'CateController@search');
    Route::get('about', 'HomeController@about_page');
    Route::get('api', 'HomeController@api_page');
    Route::get('all', 'CateController@all_page');
    Route::get('rank', 'QuizController@quiz_rank');
    Route::get('quiz/new', 'Admin\Quiz@add_quiz');
    Route::post('quiz/new', 'Admin\Quiz@add_quiz_save');
    Route::get('quiz', 'QuizController@index');
    Route::get('quiz/list', 'QuizController@index_all');
    Route::get('quiz/category/{slug}', 'QuizController@category');
    Route::get('quiz/play/{id}', 'QuizController@play');
    Route::post('quiz/play/{id}', 'QuizController@save_play');
    Route::get('quiz/result/{id_key}', 'QuizController@result_log');
    Route::get('quiz/history', 'QuizController@quiz_history');
    Route::get('quiz/rank', 'QuizController@quiz_rank');
    Route::get('profile', 'HomeController@profile');
    Route::post('profile', 'HomeController@profile_save');
    Route::get('profile/password', 'HomeController@change_password');
    Route::post('profile/password', 'HomeController@password_save');
    Route::get('{type}/{id}', 'CateController@view_tag');
});